<!-- omit in toc -->
# Contributing to Jacamar CI

First off, thanks for taking the time to contribute! ❤️

All types of contributions are encouraged and valued. See the
[Table of Contents](#table-of-contents) for different ways to help and details
about how this project handles them. Please make sure to read the relevant
section before making your contribution. It will make it a lot easier for
us maintainers and smooth out the experience for all involved. The community
looks forward to your contributions. 🎉

<!-- omit in toc -->
## Table of Contents

- [I Have a Question](#i-have-a-question)
- [I Want To Contribute](#i-want-to-contribute)
- [Reporting Bugs](#reporting-bugs)
- [Suggesting Enhancements](#suggesting-enhancements)
- [Code Contributions](#code-contributions)

## I Have a Question

> If you want to ask a question, we assume that you have read the
  available [Documentation](https://ecp-ci.gitlab.io).

Before you ask a question, it is best to search for existing 
[Issues](https://gitlab.com/ecp-ci/jacamar-ci/-/issues) that might help you.
In case you have found a suitable issue and still need clarification, you
can write your question in this issue.

If you then still feel the need to ask a question and need clarification,
we recommend the following:

- Open an [Issue](https://gitlab.com/ecp-ci/jacamar-ci/-/issues/new?issuable_template=Question)
  with the `Question` template
- Provide as much context as you can about what you're running into.
- Provide project and platform versions, depending on what seems relevant.
- Link to any related resources you've used or are related to the question.

We will then take care of the issue as soon as possible.

### Questions About GitLab

We are unable to answer questions that are strictly related to the GitLab
runner/server.

## I Want To Contribute

### Reporting Bugs

<!-- omit in toc -->
#### Before Submitting a Bug Report

A good bug report should contain enough information for maintainers to troubleshoot
the issue. Therefore, we ask you to investigate carefully, collect information
and describe the issue in detail in your report. Please complete the following
steps in advance to help us fix any potential bug as fast as possible.

- Make sure that you are using the
  [latest release](https://ecp-ci.gitlab.io/docs/admin.html#latest-releases).
- Determine if your bug is really a bug and not an error on your side e.g.
  using incompatible environment components/versions (Make sure that you have read 
  the [deployment documentation](https://ecp-ci.gitlab.io/docs/admin/jacamar/deployment.html)).
- To see if other users have experienced (and potentially already solved)
  the same issue you are having, check if there is not already a bug
  report existing for your bug or error in the
  [bug tracker](https://gitlab.com/ecp-ci/jacamar-ci/-/issues/?sort=created_date&state=opened&label_name%5B%5D=bug&first_page_size=20).
- Collect and provide project and platform versions.
- If possible, include your input and the output of the CI job. Be careful to sanitize
  all data before posting to ensure secrets and/or other sensitive information is not included.
- Can you reliably reproduce the issue? And can you also reproduce it 
  with older versions?

<!-- omit in toc -->
#### How Do I Submit a Good Bug Report?

> You must never report security related issues, vulnerabilities or bugs including
  sensitive information in public view. Instead, sensitive bugs
  should be opened via a [confidential](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html)
  [Security Issue](https://gitlab.com/ecp-ci/jacamar-ci/-/issues/new?issuable_template=Security)

We use GitLab issues to track bugs and errors. If you run into an issue with
the project:

- Open an [Issue](https://gitlab.com/ecp-ci/jacamar-ci/-/issues/new?issuable_template=Bug).
- Explain the behavior you would expect and the actual behavior.
- Please provide as much context as possible and describe the
  *Steps to reproduce* that someone else can follow to recreate the issue
  on their own. This usually includes your code. For good bug reports you
  should isolate the problem and create a reduced test case.
- Provide the information you collected in the previous section.
- If there is a potential fix you feel appropriate please include any
  supporting links/details.

Once it's filed:

- The project team will label/update the issue accordingly.
- A team member will try to reproduce the issue with your provided steps
  if possible.
- If there are no included steps or no obvious way to reproduce the issue, the
  team will ask you for those steps and mark the issue as `info required`.

> There will be cases where it is not possible for a team member to replicate
  the issue as documented due to a number of potential unique features and/or
  licensed components in your environment. In those cases please gather
  and provide as much logging as possible, see the
  [Introduction to Local System Logging](https://ecp-ci.gitlab.io/docs/guides/admin-job-tracing.html)
  guide for complete details. Though be warned you should take care to sanitize
  any logs before sharing them publicly.

### Suggesting Enhancements

This section guides you through submitting an enhancement suggestion
for Jacamar CI, **including completely new features and minor improvements to
existing functionality**. Following these guidelines will help maintainers
and the community to understand your suggestion and find related suggestions.

<!-- omit in toc -->
#### Before Submitting an Enhancement

- Make sure that you are using the latest version.
- Read the [documentation](https://ecp-ci.gitlab.io) carefully and find out
  if the functionality is already covered, maybe by an individual configuration.
- Perform a [search](https://gitlab.com/ecp-ci/jacamar-ci/-/issues) to see if
  the enhancement has already been suggested. If it has, add a comment to the
  existing issue instead of opening a new one.
- Find out whether your idea fits with the scope and aims of the project. It's
  up to you to make a strong case to convince the project's developers of the
  merits of this feature. Keep in mind that we want features that will be useful 
  to the majority of our users and not just a small subset.

<!-- omit in toc -->
#### How Do I Submit a Good Enhancement Suggestion?

Enhancement suggestions are tracked as
[GitLab issues](https://gitlab.com/ecp-ci/jacamar-ci/-/issues/new?issuable_template=Enhancement).

- Use a **clear and descriptive title** for the issue to
  identify the suggestion.
- Provide a **step-by-step description of the proposed enhancement** in as
  many details as possible.
- **Explain why this enhancement would be useful** to users. You may also
  want to point out the other projects that solved it better and which
  could serve as inspiration.
- Highlight any benefits as well as potential risks associated with
  adding this enhancement.

### Code Contributions

#### Local Development Environment

We advise using [asdf](https://asdf-vm.com/) to manage  all *required*
dependencies for your local development environment. If this is not possible
please refer to the `.tool-versions` file to identify all software requirements
and their associated versions you will then need to account for these manually.

Additionally, it can be beneficial to install a local container runtime
like [Podman](https://podman.io/) or [Docker](https://www.docker.com/).
This can be used with commands such as `make test-container` or any other
Make command containing the `container` target. In all these cases the
entire action will run in a pre-defined image with all build/runtime
dependencies accounted for.

#### Improving The Documentation

Additional documentation for the project is available on
[ecp-ci.gitlab.io](https://ecp-ci.gitlab.io/) and the source
code can be found in the [ecp-ci.gitlab.io](https://gitlab.com/ecp-ci/ecp-ci.gitlab.io)
project repository. Please see the project's associated
[CONTRIBUTING.md](https://gitlab.com/ecp-ci/ecp-ci.gitlab.io/-/blob/main/CONTRIBUTING.md)
file for additional details.

## Attribution

This guide is based on the **contributing-gen**.
[Make your own](https://github.com/bttger/contributing-gen)!
