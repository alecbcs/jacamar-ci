// SPDX-License-Identifier: (Apache-2.0 OR MIT)

// Package gljobctx offers a workflow with optional controls to validate a
// CI_JOB_JWT.
//
// Example code:
//
//	encoded := os.Getenv("CI_JOB_JWT")
//	server := os.Getenv("CI_SERVER_URL")
//
//	claims, err := gljobctx.Options{}.ValidateJWT(encoded, server)
//	if err != nil {
//	    log.Fatal(err)
//	}
//	log.Printf("JWT valid for JobID: %s\n", claims.JobID)
package gljobctx

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"gitlab.com/ecp-ci/gljobctx-go/internal/jwks"
	"gitlab.com/ecp-ci/gljobctx-go/internal/web"
)

const (
	maxDelay = 30 * time.Minute
)

// Options offers optional configurations that can be used to influence the
// Validator creation/workflows.
type Options struct {
	// JobID used to ensure the provided JWT aligns with a second source.
	JobID string
	// TLSCAFile file container necessary certificates for HTTPS communications.
	TLSCAFile string
	// ExpDelay indicates any potentially allowed delay in the expiration.
	ExpDelay time.Duration
	// ClaimsEnvValidation enables a secondary validation for every value found in the code
	// Claims structure that can end up in a shell environment ('env' structure tag). This occurs
	// in addition to any server communication and checks for potentially invalid characters
	// enforced upstream. It is import to note that this does not guarantee any such value
	// is correct and that stricter rules maybe necessary (based upon your systems underlying
	// requirements).
	ClaimsEnvValidation bool
	// RequiredAudience when defined enforces that the 'aud' matches
	// (https://tools.ietf.org/html/rfc7519#section-4.1).
	RequiredAudience string

	caCertPool *x509.CertPool
}

// Validater defines a shared set of procedures that observe a requesters'
// configuration to validate and establish trusted CI job context.
type Validater interface {
	ValidateJWT(string, string) (Claims, error)
}

type job struct {
	encoded            string
	jobID              string
	jwksURL            string
	requiredAud        string
	expDelay           time.Duration
	claimEnvValidation bool

	client web.Client
}

func (o Options) ValidateJWT(encoded, serverURL string) (Claims, error) {
	if o.TLSCAFile != "" {
		cf, err := os.ReadFile(o.TLSCAFile)
		if err != nil {
			return Claims{}, fmt.Errorf("invalid TLS CA file supplied: %w", err)
		}

		certPool := x509.NewCertPool()
		certPool.AppendCertsFromPEM(cf)
		o.caCertPool = certPool
	}

	// Always cap the maximum delay that can be observed.
	if o.ExpDelay > maxDelay {
		return Claims{}, fmt.Errorf("configured ExpDelay surpasses maximum %v duration", maxDelay)
	}

	return o.build(encoded, serverURL).processJWT()
}

func (j job) processJWT() (Claims, error) {
	header, err := jwks.CheckHeader(j.encoded)
	if err != nil {
		return Claims{}, fmt.Errorf("unable to validate JWT header: %w", err)
	}

	key, err := jwks.FetchKey(j.client, j.jwksURL, header.Kid, header.Alg)
	if err != nil {
		return Claims{}, newErrorJWKS(err)
	}

	return j.parseClaims(key)
}

func (o Options) build(encoded, serverURL string) job {
	u, _ := url.Parse(serverURL)
	u.Path = strings.TrimRightFunc(u.Path, func(r rune) bool {
		return r == '/'
	}) + "/oauth/discovery/keys"

	return job{
		encoded:            encoded,
		jobID:              o.JobID,
		jwksURL:            u.String(),
		requiredAud:        o.RequiredAudience,
		client:             o.web(),
		expDelay:           o.ExpDelay,
		claimEnvValidation: o.ClaimsEnvValidation,
	}
}

func (o Options) web() web.Client {
	if o.caCertPool != nil {
		return web.CustomClient(&http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					RootCAs:    o.caCertPool,
					MinVersion: tls.VersionTLS12,
				},
			},
		})
	}

	return web.DefaultClient()
}
