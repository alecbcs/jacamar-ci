# Job Context Validation

[![pipeline status](https://gitlab.com/ecp-ci/gljobctx-go/badges/main/pipeline.svg)](https://gitlab.com/ecp-ci/gljobctx-go/-/commits/main)
[![coverage report](https://gitlab.com/ecp-ci/gljobctx-go/badges/main/coverage.svg)](https://gitlab.com/ecp-ci/gljobctx-go/-/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/ecp-ci/gljobctx-go)](https://goreportcard.com/report/gitlab.com/ecp-ci/gljobctx-go)

A Go package for validating configurable
[id_tokens](https://docs.gitlab.com/ee/ci/yaml/index.html#id_tokens)
that can be found in a GitLab pipeline.

## Example

Job defined in the `.gitlab-ci.yml`:

```yaml
gotest:
  id_tokens:
    TEST_ID_JWT:
      aud: https://gitlab.com/ecp-ci
```

Validate and return claims with this library:

```go
    encoded := os.Getenv("TEST_ID_JWT")

    claims, err := gljobctx.Options{}.ValidateJWT(encoded, "https://gitlab.com")
    if err != nil {
        log.Fatal(err)
    }
	
    log.Printf("JWT valid for JobID: %s\n", claims.JobID)
```
