## Details

<!-- Any details regarding the question. -->

## Software Versions

```shell
$ /opt/jacamar/bin/jacamar --version
  ...
  
$ gitlab-runner --version
  ...
```

GitLab Server: <!-- https://your-gitlab-url/help -->

## Related Documentation/Guides

<!-- If you have been following any existing guides/examples found on https://ecp-ci.gitlab.io or other publicly available resources. -->

/label ~question
