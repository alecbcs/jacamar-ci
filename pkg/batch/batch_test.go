package batch

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

type batchTests struct {
	job      Job
	settings Settings
	batch    configure.Batch
	arg      string
	msg      *mock_logging.MockMessenger

	prepFile func(t *testing.T, s string)

	assertError   func(t *testing.T, err error)
	assertTime    func(t *testing.T, dur time.Duration)
	assertString  func(t *testing.T, s string)
	assertManager func(t *testing.T, mng Manager)
}

func TestJob_checkIllegalArgs(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(
		gomock.Eq(`Illegal argument detected. Please remove: --Error=foo`),
	)
	m.EXPECT().Warn(
		gomock.Eq("Illegal argument detected. Please remove: -o --Time=1h"),
	)

	tests := map[string]batchTests{
		"empty illegal arguments and empty user arguments": {
			job: Job{},
			msg: m, // none
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"arguments defined but not illegal argument detected": {
			job: Job{
				illegalArgs: []string{"-o", "-eo", "--Error", "--Time"},
				userArgs:    []string{"-a", "test", "-b"},
			},
			msg: m, // none
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"multiple illegal arguments found": {
			job: Job{
				illegalArgs: []string{"-o", "-eo", "--Error", "--Time"},
				userArgs:    []string{"-o", "output", "-b", "--Time=1h"},
			},
			msg: m,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "illegal arguments must be addressed before continuing")
			},
		},
		"single illegal argument found": {
			job: Job{
				illegalArgs: []string{"-o", "-eo", "--Error", "--Time"},
				userArgs:    []string{"-a", "test", "--Error=foo"},
			},
			msg: m,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "illegal arguments must be addressed before continuing")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.job.checkIllegalArgs(tt.msg)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_NewBatchJob(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	emptyMsg := mock_logging.NewMockMessenger(ctrl)

	tests := map[string]batchTests{
		"valid Manager created, no SchedulerBin or ArgumentsVariable defined": {
			settings: Settings{
				BatchCmd:    "batch",
				StateCmd:    "state",
				StopCmd:     "stop",
				IllegalArgs: []string{"-a"},
			},
			batch: configure.Batch{},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Warn(
					"No %s variable detected, please check your CI job if this is unexpected.",
					"SCHEDULER_PARAMETERS",
				).Times(1)
				return m
			}(),
			assertManager: func(t *testing.T, mng Manager) {
				assert.Equal(t, mng.BatchCmd(""), "batch")
				assert.Equal(t, mng.StateCmd(), "state")
				assert.Equal(t, mng.StopCmd(), "stop")
				assert.Equal(
					t,
					mng.UserArgs(),
					"",
					"no user arguments should be defined",
				)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"valid Manager created, SchedulerBin and ArgumentsVariables defined": {
			settings: Settings{
				BatchCmd: "batch",
				StateCmd: "state",
				StopCmd:  "stop",
			},
			batch: configure.Batch{
				SchedulerBin:      "/ci",
				ArgumentsVariable: []string{"PARAMS"},
			},
			msg: emptyMsg,
			arg: "--foo=bar -a",
			assertManager: func(t *testing.T, mng Manager) {
				assert.Equal(t, mng.BatchCmd("--foo=bar -a"), "/ci/batch --foo=bar -a")
				assert.Equal(t, mng.StateCmd(), "/ci/state")
				assert.Equal(t, mng.StopCmd(), "/ci/stop")
				assert.Equal(t, mng.UserArgs(), "--foo=bar -a")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"valid Manager created, SchedulerBin but no ArgumentsVariables": {
			settings: Settings{
				BatchCmd: "batch",
				StateCmd: "state",
				StopCmd:  "stop",
			},
			batch: configure.Batch{
				SchedulerBin: "/ci/",
			},
			msg: emptyMsg,
			arg: "--foo=bar -a",
			assertManager: func(t *testing.T, mng Manager) {
				assert.Equal(t, mng.BatchCmd("--foo=bar -a"), "/ci/batch --foo=bar -a")
				assert.Equal(t, mng.StateCmd(), "/ci/state")
				assert.Equal(t, mng.StopCmd(), "/ci/stop")
				assert.Equal(t, mng.UserArgs(), "--foo=bar -a")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"multiple batch environment keys defined": {
			settings: Settings{
				BatchCmd: "batch",
				StateCmd: "state",
				StopCmd:  "stop",
			},
			batch: configure.Batch{
				SchedulerBin: "/ci",
				EnvVars: []string{
					"HELLO=WORLD",
					"CFG=FILE",
				},
			},
			msg: emptyMsg,
			arg: "--foo=bar -a",
			assertManager: func(t *testing.T, mng Manager) {
				assert.Equal(
					t,
					mng.BatchCmd("--custom"),
					"export HELLO=WORLD && export CFG=FILE && /ci/batch --custom",
				)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"allow illegal arguments": {
			settings: Settings{
				BatchCmd:    "batch",
				IllegalArgs: []string{"-J", "--job-name"},
			},
			batch: configure.Batch{
				AllowIllegalArgs:  true,
				ArgumentsVariable: []string{"PARAMS"},
			},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Warn(
					gomock.Eq("Illegal argument detected. Please remove: --job-name=test"),
				)
				return m
			}(),
			arg: "--foo=bar -a --job-name=test",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.arg != "" {
				if len(tt.batch.ArgumentsVariable) != 0 {
					t.Setenv(envkeys.UserEnvPrefix+tt.batch.ArgumentsVariable[0], tt.arg)
				} else {
					t.Setenv(envkeys.UserEnvPrefix+"SCHEDULER_PARAMETERS", tt.arg)
				}
			}

			mng, err := NewBatchJob(tt.settings, tt.batch, tt.msg)

			if tt.assertManager != nil {
				tt.assertManager(t, mng)
			}
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestJob_NFSTimeout(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(
		"Invalid nfs_timeout configuration specified by runner configuration: %s",
		"time: invalid duration \"string\"",
	).Times(1)

	tests := map[string]batchTests{
		"valid time duration string provided": {
			arg: "1s",
			msg: m,
		},
		"invalid time duration string provided": {
			arg: "string",
			msg: m,
		},
		"empty string provided": {
			arg: "",
			msg: m,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			tt.job.NFSTimeout(tt.arg, tt.msg)
		})
	}
}

func TestJob_MonitorTermination(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	run := mock_runmechanisms.NewMockRunner(ctrl)
	run.EXPECT().RequestContext().Return(ctx).AnyTimes()
	run.EXPECT().ReturnOutput("cancel 101").Return("output", errors.New("error message"))

	t.Run("routine closed after job is completed", func(t *testing.T) {
		jobDone := make(chan struct{})

		j := Job{
			stopCmd: "cancel",
		}
		go func() {
			time.Sleep(100 * time.Millisecond)
			jobDone <- struct{}{}
		}()
		j.MonitorTermination(run, jobDone, "1", t.TempDir())
	})

	t.Run("job canceled and error encountered", func(t *testing.T) {
		jobDone := make(chan struct{})
		defer close(jobDone)

		j := Job{
			stopCmd: "cancel",
		}
		go func() {
			time.Sleep(100 * time.Millisecond)
			cancel()
			time.Sleep(2 * time.Second)
		}()
		j.MonitorTermination(run, jobDone, "101", t.TempDir())
	})
}

func TestCommandDelay(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(
		"Invalid command_delay specified by runner configuration: %s",
		gomock.Any(),
	).Times(1)

	tests := map[string]batchTests{
		"valid time duration string provided": {
			arg: "1s",
			msg: m,
			assertTime: func(t *testing.T, dur time.Duration) {
				assert.Equal(t, 1*time.Second, dur)
			},
		},
		"invalid time duration string provided": {
			arg: "string",
			msg: m,
			assertTime: func(t *testing.T, dur time.Duration) {
				assert.Equal(t, defaultSleep, dur)
			},
		},
		"empty string provided": {
			arg: "",
			msg: m,
			assertTime: func(t *testing.T, dur time.Duration) {
				assert.Equal(t, defaultSleep, dur)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := CommandDelay(tt.arg, tt.msg)

			if tt.assertTime != nil {
				tt.assertTime(t, got)
			}
		})
	}
}
