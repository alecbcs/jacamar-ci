package configure

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/BurntSushi/toml"
)

const (
	ansiBlue  = "\033[0;34m"
	ansiReset = "\033[0;m"
)

// Config maintains all identified options applicable to the running job.
type Config struct {
	opt Options
}

// NewConfig accepts a target configuration, either a complete file path
// or configure.EnvVariable. The appropriate contents of the associated
// target will be loaded and made available through the returned interface.
func NewConfig(tar string) (cfg Config, err error) {
	if tar == EnvVariable {
		encoded := os.Getenv(EnvVariable)
		var decoded []byte
		decoded, err = base64.StdEncoding.DecodeString(encoded)
		if err != nil {
			return Config{}, fmt.Errorf(
				"error executing DecodeString() on %s contents, %w", EnvVariable, err,
			)
		}
		cfg.opt, err = loadContents(string(decoded))
	} else {
		cfg.opt, err = loadFile(tar)
	}

	return
}

// PrepareState creates an encoded TOML (string) for usage as a environment
// variable to be provided to another process. By declaring scope any
// [Auth] related configuration will be removed.
func (c Config) PrepareState(scope bool) (string, error) {
	if scope {
		c.opt = Options{
			General: c.opt.General,
			Batch:   c.opt.Batch,
		}
	}
	return encode(c.opt)
}

// PrepareNotification generates and returns a valid message with key aspects of the
// configuration safe to convey to the user.
func (c Config) PrepareNotification(scriptDir string) string {
	var sb strings.Builder

	sb.WriteString("Targeting " + c.opt.General.Executor + " executor\n")

	if c.opt.General.CustomBuildDir {
		sb.WriteString("Custom builds directory enabled (set with CUSTOM_CI_BUILDS_DIR variable)\n")
	}
	if c.opt.General.RetainLogs {
		sb.WriteString("Job and scheduler logs retained automatically (")
		sb.WriteString(scriptDir)
		sb.WriteString(")\n")
	}
	if c.opt.Auth.RunAs.RunAsVariable != "" {
		sb.WriteString("RunAs service account process enabled (propose with ")
		sb.WriteString(c.opt.Auth.RunAs.RunAsVariable)
		sb.WriteString(" variable)\n")
	}
	if c.opt.General.StaticBuildsDir {
		sb.WriteString("Static builds directory in use, all jobs results kept. ")
		sb.WriteString("Please be aware of large disk space requirements when using this runner")
		if c.opt.General.StaticMinDays != 0 {
			sb.WriteString(fmt.Sprintf(" (minimum days: %d)", c.opt.General.StaticMinDays))
		}
		sb.WriteString("\n")
	}

	// Note regarding any action that may cause close down during cleanup_exec. We will
	// not be able to provide these details during the cleanup stage.
	if c.opt.General.MaxBuildDir > 2 || c.opt.General.StaticCleanupLimit > 2 {
		sb.WriteString(ansiBlue + "Performance Notification" + ansiReset)
		sb.WriteString(": Additional directory cleanup required due to configuration.\n")
		sb.WriteString("Any additional cleanup may extend the job's duration, please note this will not")
		sb.WriteString(" come at the cost of your compute time but may slightly delay subsequent jobs.\n")
	}

	if c.opt.General.RunMechanism != "" {
		sb.WriteString(fmt.Sprintf("Run mechanism %s ", c.opt.General.RunMechanism))
		if !c.opt.General.ForceMechanism {
			sb.WriteString("optionally ")
		}
		sb.WriteString("enabled\n")
	}

	return sb.String()
}

// Options returns the contents of a decoded configuration.
func (c Config) Options() Options {
	return c.opt
}

// General return the structure of a decoded configuration.
func (c Config) General() General {
	return c.opt.General
}

// Auth return the structure of a decoded configuration.
func (c Config) Auth() Auth {
	return c.opt.Auth
}

// Batch return the structure of a decoded configuration.
func (c Config) Batch() Batch {
	return c.opt.Batch
}

func MetaOptions(file string) (toml.MetaData, Options, error) {
	opt := defaults()
	b, err := os.ReadFile(filepath.Clean(file))
	if err != nil {
		return toml.MetaData{}, opt, err
	}

	r := strings.NewReader(string(b))
	meta, err := toml.NewDecoder(r).Decode(&opt)

	return meta, opt, err
}

func defaults() Options {
	return Options{
		Auth: Auth{
			MaxEnvChars: defMaxEnvChars,
			JWTExpDelay: defJWTExpDelay,
			Logging: Logging{
				Level:    defLogLevel,
				Location: defLogLocation,
			},
		},
		Batch: Batch{
			NFSTimeout: defNFSTimeout,
		},
		General: General{
			KillTimeout:        defKillTimeout,
			StaticMinDays:      defStaticMinDays,
			StaticCleanupLimit: defStaticCleanupLimit,
		},
	}
}

// loadFile loads the specified TOML file and decodes the contents
// into the Options struct.
func loadFile(file string) (Options, error) {
	r, err := os.ReadFile(filepath.Clean(file))
	if err != nil {
		return Options{}, fmt.Errorf("error executing ReadFile(), %w", err)
	}

	return loadContents(string(r))
}

// loadContents decodes the already read contents of a TOML file into the Options struct.
func loadContents(contents string) (Options, error) {
	if contents == "" {
		return Options{}, fmt.Errorf(
			"missing configuration, expected in runner environment (%s)", EnvVariable,
		)
	}

	cfg := defaults()
	r := strings.NewReader(contents)
	_, err := toml.NewDecoder(r).Decode(&cfg)
	if err != nil {
		return Options{}, fmt.Errorf("error executing DecodeReader(), %w", err)
	}

	return cfg, nil
}

// encode returns the TOML of the options as a string in Base64.
func encode(opt Options) (string, error) {
	buf := new(bytes.Buffer)
	err := toml.NewEncoder(buf).Encode(opt)
	return base64.StdEncoding.EncodeToString(buf.Bytes()), err
}
