package envparser

import (
	"encoding/base64"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

// encodeRegistryCredentials encodes all registry credentials provided by the GitLab
// service in the job response in a format that can be passed to future stages via
// the StatefulEnv. Please note these are not encrypted and Base64 encoded.
func (j JobResponse) encodeRegistryCredentials() string {
	s := []string{}
	for _, v := range j.Credentials {
		if v.Type == "registry" {
			s = append(s, fmt.Sprintf(
				"%s:%s",
				base64.RawURLEncoding.EncodeToString([]byte(v.URL)),
				base64.RawStdEncoding.EncodeToString([]byte(v.Username+":"+v.Password)),
			))
		}
	}

	return strings.Join(s, ",")
}

func (j JobResponse) prepareImageEntrypoint() string {
	return strings.Join(j.Image.Entrypoint, ",")
}

// DecodeRegistryCredentials returns the decoded stateful credentials in a form
// (map[url]encodedAuth) that can be used by container runtime in auth files.
func (s StatefulEnv) DecodeRegistryCredentials() map[string]string {
	encodedC := strings.Split(s.RegistryCredentials, ",")
	creds := make(map[string]string, len(encodedC))

	for _, v := range encodedC {
		tmp := strings.Split(v, ":")
		if len(tmp) != 2 {
			// Avoid panic if such a case did occur.
			continue
		}
		bURL, _ := base64.RawURLEncoding.DecodeString(tmp[0])
		creds[string(bURL)] = tmp[1]
	}

	return creds
}

func UserVolumes(gen configure.General) (s []string, err error) {
	if gen.Podman.UserVolumeVariable == "" {
		return
	}

	env := os.Environ()
	for _, v := range env {
		if strings.HasPrefix(v, envkeys.UserEnvPrefix+gen.Podman.UserVolumeVariable) {
			split := strings.Split(v, "=")
			key := strings.TrimPrefix(split[0], envkeys.UserEnvPrefix)

			if split[1] == "" {
				err = fmt.Errorf("no volume detected for %s", key)
			}

			tmpPath := filepath.Clean(split[1])
			if strings.ContainsAny(tmpPath, "! | $") {
				err = fmt.Errorf("invalid characters detected in %s", key)
			}

			s = append(s, tmpPath)
		}
	}

	return
}
