package envparser

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

func Test_StatefulEnv_DecodeRegistryCredentials(t *testing.T) {
	tests := map[string]envTests{
		"invalid stateful variable": {
			stateful: StatefulEnv{
				RegistryCredentials: "ZXhhbXBsZS5jb20:abc,invalid",
			},
			assertMap: func(t *testing.T, m map[string]string) {
				assert.Len(t, m, 1)
				assert.Equal(t, "abc", m["example.com"])
			},
		},
		"multiple valid": {
			stateful: StatefulEnv{
				RegistryCredentials: "ZXhhbXBsZS5jb20:dXNlcjpwYXNz,cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
			},
			assertMap: func(t *testing.T, m map[string]string) {
				assert.Len(t, m, 2)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := tt.stateful.DecodeRegistryCredentials()

			if tt.assertMap != nil {
				tt.assertMap(t, got)
			}
		})
	}
}

func TestUserVolumes(t *testing.T) {
	tests := map[string]envTests{
		"invalid variable provided": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "TEST_VAR_0": "$test",
			},
			opt: configure.Options{
				General: configure.General{
					Podman: configure.Podman{
						UserVolumeVariable: "TEST_VAR",
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "invalid characters")
			},
		},
		"missing value": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "TEST_VAR_0": "/var/test:/opt:z",
				envkeys.UserEnvPrefix + "TEST_VAR_1": "",
			},
			opt: configure.Options{
				General: configure.General{
					Podman: configure.Podman{
						UserVolumeVariable: "TEST_VAR",
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "no volume detected for TEST_VAR_1")
			},
		},
		"multiple valid paths": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "TEST_VAR_0": "/var/test:/opt:z",
				envkeys.UserEnvPrefix + "TEST_VAR_1": "/test",
			},
			opt: configure.Options{
				General: configure.General{
					Podman: configure.Podman{
						UserVolumeVariable: "TEST_VAR",
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertSlice: func(t *testing.T, s []string) {
				assert.Len(t, s, 2)
			},
		},
		"skip process": {
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertSlice: func(t *testing.T, s []string) {
				assert.Len(t, s, 0)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			got, err := UserVolumes(tt.opt.General)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertSlice != nil {
				tt.assertSlice(t, got)
			}
		})
	}
}
