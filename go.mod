module gitlab.com/ecp-ci/jacamar-ci

go 1.21

require (
	github.com/BurntSushi/toml v1.3.2
	github.com/go-playground/validator/v10 v10.18.0
	github.com/golang/mock v1.6.0
	github.com/seccomp/libseccomp-golang v0.10.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.8.4
	gitlab.com/ecp-ci/gljobctx-go v0.7.2
	golang.org/x/sys v0.17.0
	kernel.org/pub/linux/libs/security/libcap/cap v1.2.69
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gabriel-vasile/mimetype v1.4.3 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/golang-jwt/jwt/v5 v5.2.0 // indirect
	github.com/leodido/go-urn v1.4.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/crypto v0.19.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	kernel.org/pub/linux/libs/security/libcap/psx v1.2.69 // indirect
)
