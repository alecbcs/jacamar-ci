package preparations

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"reflect"
	"strconv"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/internal/version"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

// proj represents the excepted json STDOUT of the config_exec stage.
type proj struct {
	Builds   string            `json:"builds_dir"`
	Cache    string            `json:"cache_dir"`
	Share    bool              `json:"builds_dir_is_shared"`
	Hostname string            `json:"hostname"`
	State    map[string]string `json:"job_env"`
	Driver   map[string]string `json:"driver"`
}

// ConfigExec realizes the config_exec custom executor stage. It will run without leveraging
// any authorized user, instead valid JSON payload is created for the upstream runner to ingest.
// No previously established configuration or system settings will be modified.
func (f Factory) ConfigExec(auth authuser.Authorized, exit func()) {
	js, err := f.configJSON(auth)
	if err != nil {
		errMsg := fmt.Sprintf("unable to construct job configuration payload: %s", err.Error())
		StdError(f.Args, errMsg, f.AbsExec.Msg, exit)
		return
	}

	f.AbsExec.Msg.Stdout(js) // Print configuration.
	f.SysLog.Debug(fmt.Sprintf("configuration payload established'"))
}

// configJSON encodes the included proj struct as json and returns it as a string for stdout.
func (f Factory) configJSON(auth authuser.Authorized) (string, error) {
	// Failure to retrieve hostname (resulting in empty string)
	// does not present any risk.
	host, _ := os.Hostname()

	p := proj{
		Share:    false,
		Hostname: host,
		Driver: map[string]string{
			"name":    "Jacamar CI",
			"version": version.Version(),
		},
	}

	if err := f.updateDirs(&p, auth); err != nil {
		return "", err
	}

	state, err := f.buildState(auth)
	if err != nil {
		return "", err
	}

	// Configuration (managed outside StatefulEnv structure) and potential
	// directory overrides.
	state[configure.EnvVariable] = f.AbsExec.ExecOptions
	state["JACAMAR_CI_BUILDS_DIR"] = p.Builds
	p.State = state

	data, err := json.Marshal(&p)

	return string(data), err
}

// buildState generates a mapping of values to StatefulEnv keys.
func (f Factory) buildState(auth authuser.Authorized) (map[string]string, error) {
	state := auth.BuildState()
	f.AbsExec.Env.JobResponse.ExpandState(&state)

	env := make(map[string]string)

	t := reflect.TypeOf(&state).Elem()
	v := reflect.ValueOf(&state).Elem()
	for i := 0; i < t.NumField(); i++ {
		fieldT := t.Field(i)
		fieldV := v.Field(i)

		tag := fieldT.Tag.Get(envparser.UserVarTagName)
		req := fieldT.Tag.Get(envparser.RequiredKey)

		if req == "true" && fieldV.String() == "" {
			return env, fmt.Errorf("unable to identify required stateful variable %s", tag)
		} else if req == "false" && fieldV.String() == "" {
			continue
		}

		env[tag] = fieldV.String()
	}

	return env, nil
}

func (f Factory) updateDirs(p *proj, auth authuser.Authorized) (err error) {
	p.Builds = auth.CIUser().BuildsDir
	p.Cache = auth.CIUser().CacheDir

	if f.AbsExec.Cfg.General().FFLimitBuildDir {
		var dir string
		dir, err = f.identifyConcurrent(auth)
		p.Builds = p.Builds + "/" + dir
		if err != nil {
			return
		}
	}

	return
}

func (f Factory) identifyConcurrent(auth authuser.Authorized) (string, error) {
	ctx := context.Background()
	defer ctx.Done()

	f.Args = arguments.ConcreteArgs{
		Lock: &arguments.LockCmd{
			ProposedDir:    auth.CIUser().BuildsDir,
			JobID:          f.AbsExec.Env.JobID,
			JobTimeout:     strconv.Itoa(f.AbsExec.Env.JobResponse.RunnerInfo.Timeout),
			DirPermissions: prepPermissions(f.AbsExec.Cfg.General()),
		},
	}

	// Build and use a new commander interface to ensure that any configured
	// underlying downscope methods are observed. Our new ConcreteArgs will
	// be utilized when constructing the command.
	cmdr, err := f.NewCommander(ctx, auth)
	if err != nil {
		return "", err
	}

	out, cmdErr := cmdr.ReturnOutput("")
	out = strings.TrimSpace(out)
	_, strErr := strconv.Atoi(out)
	if strErr != nil || cmdErr != nil {
		// Generate new error to avoid using unknown downscope output.
		err = errors.New(
			"jacamar lock command failed, verify user application permissions and " +
				"no additional output injected into downscope command's stdout",
		)
	}

	return out, err
}

func prepPermissions(gen configure.General) string {
	// In order to simply passing the target via CLI regardless of downscope we
	// will convert permissions to a numerical uint string. The creation as well
	// as prepare_exec will enforce secure defaults in case of errors.
	mode := usertools.IdentifyPermissions(gen)
	return strconv.FormatUint(uint64(mode), 10)
}
