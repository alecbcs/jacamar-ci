// Package authuser (authorize user) maintains the interface for the complete authorization
// flow, leveraging the job's configuration as well as context provided by the custom executor
// to ensure a fully authorized user is identified for the local job.
package authuser

import (
	"fmt"
	"os/user"
	"strconv"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/validation"
	"gitlab.com/ecp-ci/jacamar-ci/internal/jobtoken"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

// Authorized implements an interface allowing read-only access to the established user
// context. All information is identified during a job's configuration stage and remains
// consistent throughout the life of the job.
type Authorized interface {
	// CIUser returns a completely authorized user context.
	CIUser() UserContext
	// BuildState populates and return the StatefulEnv.
	BuildState() envparser.StatefulEnv
	// PrepareNotification returns details on the validated user for prepare_exec messaging.
	PrepareNotification() string
	// JobContext returns a subset of verified job claims for further access. Note this is
	// only present when authorization has not been disabled.
	JobContext() jobtoken.EstablishedContext
}

// UserContext contains validated user details for the current CI job.
type UserContext struct {
	Username string
	HomeDir  string
	UID      int
	GID      int
	// Groups all supplementary unix groups assigned to the user.
	Groups []uint32
	// BaseDir directory for CI job and command interactions.
	BaseDir string
	// BuildsDir is the working directory created on the local file system.
	BuildsDir string
	// CacheDir is the working directory created on the local file system.
	CacheDir string
	// ScriptDir is the directory for script storage and command execution.
	ScriptDir string
	// DataDirOverride proposed override for configured data_dir.
	DataDirOverride string
}

// CurrentUser returns the current user.
func CurrentUser() (*user.User, error) {
	return user.Current()
}

// ProcessFromState update basic UserContext using the supplied user object in conjunction
// with Stateful environment variables identified.
func ProcessFromState(usr *user.User, s envparser.StatefulEnv) (u UserContext, err error) {
	u.Username = usr.Username
	u.HomeDir = usr.HomeDir
	u.Username = s.Username
	u.BaseDir = s.BaseDir
	u.BuildsDir = s.BuildsDir
	u.CacheDir = s.CacheDir
	u.ScriptDir = s.ScriptDir

	err = SetIntIDs(usr.Uid, usr.Gid, &u)

	return
}

// SetIntIDs converts the supplied uid & gid to integers and updates the UserContext.
func SetIntIDs(uid, gid string, u *UserContext) error {
	iUid, err := strconv.Atoi(uid)
	if err != nil {
		return fmt.Errorf("unable to convert valid user's UID (%s)", uid)
	}
	iGid, err := strconv.Atoi(gid)
	if err != nil {
		return fmt.Errorf("unable to convert valid user's GID (%s)", gid)
	}

	u.UID = iUid
	u.GID = iGid

	return nil
}

type Validators struct {
	RunAs validation.RunAsValidator
	Job   jobtoken.Validator
}

// EstablishValidators create a series of Validators based upon configuration and identifiable job context.
func EstablishValidators(
	stage string,
	opt configure.Options,
	env envparser.ExecutorEnv,
) (Validators, error) {
	rs, err := validation.NewRunAs(opt.Auth)
	if err != nil {
		return Validators{}, fmt.Errorf("failed to establish RunAs validator: %w", err)
	}

	return Validators{
		RunAs: rs,
		Job:   jobtoken.NewJob(stage, opt, env),
	}, err
}
