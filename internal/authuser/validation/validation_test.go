package validation

import (
	"path/filepath"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

func TestNewRunAs(t *testing.T) {
	testEnv := map[string]string{
		envkeys.UserEnvPrefix + "RUNAS_USER":     "tester",
		envkeys.UserEnvPrefix + "BAD_RUNAS_USER": "$(sudo rm -rf /)",
	}

	tests := map[string]struct {
		auth        configure.Auth
		assertError func(*testing.T, error)
		assertRunAs func(*testing.T, RunAsValidator)
	}{
		"no validation script/plugin defined": {
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunAs: func(t *testing.T, r RunAsValidator) {
				assert.Nil(t, r)
			},
		},
		"invalid runas user detected in environment": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: "script",
					RunAsVariable:    "BAD_RUNAS_USER",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"invalid user provided account (defined by BAD_RUNAS_USER variable): Key: '' Error:Field validation for '' failed on the 'username' tag",
				)
			},
			assertRunAs: func(t *testing.T, r RunAsValidator) {
				assert.Nil(t, r)
			},
		},
		"script runas, no target user defined": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: "/example/script/file",
					RunAsVariable:    "",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunAs: func(t *testing.T, r RunAsValidator) {
				// We only need to verify the type here, we have other tests
				// that better examine functionality.
				assert.Equal(t, reflect.TypeOf(runAsScript{}), reflect.TypeOf(r))
			},
		},
		"script runas, target user defined": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: "/example/script/file",
					RunAsVariable:    "RUNAS_USER",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunAs: func(t *testing.T, r RunAsValidator) {
				assert.Equal(t, reflect.TypeOf(runAsScript{}), reflect.TypeOf(r))
			},
		},
		"plugins no longer supported": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationPlugin: "/example/plugin/file",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "plugin support has been removed from Jacamar, please migrate to validation_script")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range testEnv {
				t.Setenv(k, v)
			}

			got, err := NewRunAs(tt.auth)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertRunAs != nil {
				tt.assertRunAs(t, got)
			}
		})
	}
}

func Test_fileStatus(t *testing.T) {
	testScript, _ := filepath.Abs("../../../test/scripts/unit/runas.bash")

	tests := map[string]struct {
		file    string
		sha     string
		wantErr bool
	}{
		"invalid file": {
			file:    "/invalid/file/stats.exmaple",
			sha:     "",
			wantErr: true,
		},
		"valid file and invalid sha": {
			file:    testScript,
			sha:     "test",
			wantErr: true,
		},
		"valid file and sha": {
			file:    testScript,
			sha:     "a9c5d3f46b4afe03dac6f6401fa061c9e5affc57cede6d3b2fc00682b203b078",
			wantErr: false,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if err := fileStatus(tt.file, tt.sha); (err != nil) != tt.wantErr {
				t.Errorf("fileStatus() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}

	t.Run("nil file panic avoided", func(t *testing.T) {
		err := checksum(nil, "some-checksum")
		assert.EqualError(t, err, "invalid argument")
	})
}
