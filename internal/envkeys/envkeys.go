// Package envkeys maintains constants for all potentially required environment
// variable keys.
package envkeys

const (
	// UserEnvPrefix is the expected runner defined prefix for all environment variables
	// provided to the custom executor driver.
	UserEnvPrefix = "CUSTOM_ENV_"
	// StatefulEnvPrefix is the required application defined prefix for stateful (e.g. job)
	// environment variables.
	StatefulEnvPrefix = "JACAMAR_CI_"
	// ScriptContentsPrefix is the prefix for all environment variables set with encoded
	// script contents.
	ScriptContentsPrefix = "JACAMAR_SCRIPT_CONTENTS_"
	// SysExitCode is the key for the preferred system failure exit status.
	SysExitCode = "SYSTEM_FAILURE_EXIT_CODE"
	// BuildExitCode is the key for the preferred build failure exit status.
	BuildExitCode = "BUILD_FAILURE_EXIT_CODE"
	// JobResponse location for the CI job response file with full payload contents. This
	// is only accessible by the auth user.
	JobResponse = "JOB_RESPONSE_FILE"
	// CustomBuildsDir key for user proposed build directory location.
	CustomBuildsDir = "CUSTOM_CI_BUILDS_DIR"
	// CopySchedulerLogs key for user proposed location to copy batch scheduling related logs.
	CopySchedulerLogs = "COPY_SCHEDULER_LOGS"
	// RunnerTimeout key for stateful runner timeout, string duration.
	RunnerTimeout = StatefulEnvPrefix + "RUNNER_TIMEOUT"
	// JacamarShell key to indicate to user profile Jacamar CI is in use.
	JacamarShell = "JACAMAR_CI_SHELL"
	// JacamarNoProfile key indicates the users wishes to have a minimum Bash shell.
	JacamarNoProfile = "JACAMAR_NO_BASH_PROFILE"
	// SchedulerSignal optional key allowing users to declare custom signal (name
	// or number) sent to the scheduler when a job is canceled.
	SchedulerSignal = UserEnvPrefix + "SCHEDULER_CANCEL_SIGNAL"
	// JacamarNoMechanism key for a user defined variable requesting no non-standard
	// run_mechanism is used.
	JacamarNoMechanism = UserEnvPrefix + "JACAMAR_CI_NO_MECHANISM"

	CIRunnerVer = "CI_RUNNER_VERSION"
	GitTrace    = "GIT_TRACE"
)
