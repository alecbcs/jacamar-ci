package runnerinit

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

type runTests struct {
	stage string

	cmdr command.Commander
	env  envparser.ExecutorEnv
	gen  configure.General
	msg  logging.Messenger
	run  runmechanisms.Runner

	targetEnv map[string]string

	assertError  func(*testing.T, error)
	assertRunner func(*testing.T, runmechanisms.Runner)
	assertString func(*testing.T, string)
}

func TestInitializeUserRunner(t *testing.T) {
	tests := map[string]runTests{
		"unknown mechanism": {
			gen: configure.General{
				RunMechanism:   "bash",
				ForceMechanism: true,
			},
			stage: "step_script",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"unknown user variable": {
			gen: configure.General{
				RunMechanism: "podman",
			},
			stage: "step_script",
			targetEnv: map[string]string{
				envkeys.JacamarNoMechanism: "unknown",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "*basic.mechanism", reflect.TypeOf(r).String())
			},
		},
		"user override": {
			gen: configure.General{
				RunMechanism: "podman",
			},
			targetEnv: map[string]string{
				envkeys.JacamarNoMechanism: "1",
			},
			stage: "step_script",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "*basic.mechanism", reflect.TypeOf(r).String())
			},
		},
		"force run_mechanism": {
			gen: configure.General{
				RunMechanism:   "podman",
				ForceMechanism: true,
			},
			targetEnv: map[string]string{
				envkeys.JacamarNoMechanism: "1",
			},
			stage: "step_script",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "*podman.mechanism", reflect.TypeOf(r).String())
			},
		},
		"defaults no configs": {
			stage: "step_script",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "*basic.mechanism", reflect.TypeOf(r).String())
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			got, err := InitializeUserRunner(tt.cmdr, tt.env, tt.gen, tt.msg, tt.stage)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertRunner != nil {
				tt.assertRunner(t, got)
			}
		})
	}
}

func TestPrepareRunner(t *testing.T) {
	tests := map[string]runTests{
		"missing runner_image": {
			gen: configure.General{
				RunMechanism:   "podman",
				ForceMechanism: true,
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ImageName: "registry.example.io/group/test:latest",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "runner_image")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			err := PrepareRunner(tt.env, tt.gen, tt.msg, tt.run)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_idUserMech(t *testing.T) {
	tests := map[string]runTests{
		"default": {
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, defaultMech, s)
			},
		},
		"podman stateful image name": {
			gen: configure.General{
				RunMechanism: podmanMech,
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ImageName: "example:latest",
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, podmanMech, s)
			},
		},
		"podman response image name": {
			gen: configure.General{
				RunMechanism: podmanMech,
			},
			env: envparser.ExecutorEnv{
				JobResponse: envparser.JobResponse{
					Image: envparser.Image{
						Name: "example:latest",
					},
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, podmanMech, s)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := idUserMech(tt.env, tt.gen)

			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}
