package podman

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

func Test_generateAuthFile(t *testing.T) {
	tests := map[string]podmanTests{
		"no stateful credentials": {
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"invalid stateful credentials": {
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					RegistryCredentials: "testing",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "invalid registry credentials")
			},
		},
		"functional request": {
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir:           t.TempDir(),
					RegistryCredentials: "ZXhhbXBsZS5jb20:dXNlcjpwYXNz,cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				b, err := os.ReadFile(s + "/auth.json")
				assert.NoError(t, err, "unable to read auth file")

				var js json.RawMessage
				assert.NoError(t, json.Unmarshal(b, &js))
				assert.Contains(t, string(b), "dGVzdDpwYXNz")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := generateAuthFile(tt.env)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, tt.env.ScriptDir)
			}
		})
	}
}

func Test_authFileOpt(t *testing.T) {
	tmpAuthFile := t.TempDir() + "/auth.json"
	_ = os.WriteFile(tmpAuthFile, []byte{}, 0600)

	tests := map[string]podmanTests{
		"no stateful creds": {
			assertString: func(t *testing.T, s string) {
				assert.Empty(t, s)
			},
		},
		"used defined auth during get_sources": {
			userImage: false,
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + authFileKey: "/example/auth.json",
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					RegistryCredentials: "cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
					ScriptDir:           "/script/dir",
				},
			},
			stage: "get_sources",
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, " --authfile '/script/dir/auth.json'", s)
			},
		},
		"used defined auth during after_script": {
			userImage: true,
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + authFileKey: tmpAuthFile,
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					RegistryCredentials: "cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
					ScriptDir:           "/script/dir",
				},
			},
			stage: "after_script",
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, fmt.Sprintf(" --authfile '%s'", tmpAuthFile), s)
			},
		},
		"used defined auth contents": {
			userImage: true,
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + authFileKey: `{"auth":"example"}`,
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					RegistryCredentials: "cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
					ScriptDir:           t.TempDir() + "/script-test",
				},
			},
			stage: "step_script",
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "/script-test")
			},
		},
		"auth during step_script": {
			userImage: true,
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					RegistryCredentials: "cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
					ScriptDir:           "/script/dir",
				},
			},
			stage: "step_script",
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, " --authfile '/script/dir/auth.json'", s)
			},
		},
		"user defined auth during prepare_exec (w/error)": {
			userImage: true,
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + authFileKey: `{"auth":"example"}`,
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					RegistryCredentials: "cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
					ScriptDir:           t.TempDir() + "/podman-test",
				},
			},
			stage: "prepare_exec",
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "/user-auth.json")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			got := authFileOpt(tt.env, tt.stage, tt.userImage)

			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}
