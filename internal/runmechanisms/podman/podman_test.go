package podman

import (
	"context"
	"errors"
	"os"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_command"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

type podmanTests struct {
	image     string
	options   string
	src       string
	stage     string
	userImage bool

	auth configure.Auth
	cmdr command.Commander
	env  envparser.ExecutorEnv
	gen  configure.General
	msg  logging.Messenger
	run  runmechanisms.Runner

	targetEnv map[string]string

	assertError  func(*testing.T, error)
	assertRunner func(*testing.T, runmechanisms.Runner)
	assertString func(*testing.T, string)
}

func TestPrepSetup(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]podmanTests{
		"valid request with allowlist": {
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					RunnerImage: "registry.example.com/runner:version",
					ImageAllowlist: []string{
						`^registry.example.com\default/image:.*$`,
						`^registry.example.com\/group/.*$`,
					},
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ImageName: "registry.example.com/group/test:latest",
				},
			},
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().ReturnOutput(gomock.Any()).Times(2)
				return m
			}(),
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Stdout(gomock.Any()).AnyTimes()
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"invalid request with allowlist": {
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					RunnerImage: "registry.example.com/runner:version",
					ImageAllowlist: []string{
						`^registry.example.com\default/image:.*$`,
						`^registry.example.com\/group/.*$`,
					},
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ImageName: "registry.example.io/group/test:latest",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Error(gomock.Any()).Times(1)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "image not allowed")
			},
		},
		"missing runner image": {
			gen: configure.General{
				Executor:       "shell",
				ForceMechanism: true,
				Podman:         configure.Podman{},
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "runner_image")
			},
		},
		"no default image": {
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					RunnerImage: "registry.example.com/runner:version",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Error(gomock.Any()).Times(1)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "missing user image")
			},
		},
		"invalid regex": {
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					RunnerImage: "registry.example.com/runner:version",
					ImageAllowlist: []string{
						`^registry.example.com\default/image:.*$`,
						`^test[1,`,
					},
					DefaultImage: "test:latest",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "error parsing regexp")
			},
		},
		"valid request no allowlist": {
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					RunnerImage: "registry.example.com/runner:version",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ImageName: "registry.example.io/group/test:latest",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Stdout(gomock.Any()).AnyTimes()
				return m
			}(),
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().ReturnOutput(gomock.Any()).Times(2)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"failed to generate auth file": {
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					ApplicationPath: "/usr/bin/podman",
					RunnerImage:     "registry.example.com/runner:version",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ImageName:           "registry.example.io/group/test:latest",
					RegistryCredentials: "testing",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "invalid registry credentials")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := PrepSetup(tt.env, tt.gen, tt.msg, tt.run)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestNewMechanism(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]podmanTests{
		"prepare_exec user messages": {
			stage: "prepare_exec",
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					RunnerImage:        "registry.example.com/runner:version",
					UserVolumeVariable: "TEST_VOLUMES",
					StepScriptOnly:     true,
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Notify(gomock.Any()).AnyTimes()
				m.EXPECT().Stdout(gomock.Any(), gomock.Any()).AnyTimes()
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "*basic.mechanism", reflect.TypeOf(r).String())
			},
		},
		"cleanup_exec basic mechanism": {
			stage: "cleanup_exec",
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					RunnerImage: "registry.example.com/runner:version",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "*basic.mechanism", reflect.TypeOf(r).String())
			},
		},
		"step_script only": {
			stage: "step_script",
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					RunnerImage:    "registry.example.com/runner:version",
					StepScriptOnly: true,
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "*basic.mechanism", reflect.TypeOf(r).String())
			},
		},
		"after_script step only": {
			stage: "cleanup_exec",
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					RunnerImage:    "registry.example.com/runner:version",
					StepScriptOnly: true,
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "*basic.mechanism", reflect.TypeOf(r).String())
			},
		},
		"sigterm received": {
			stage: "after_script",
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					RunnerImage: "registry.example.com/runner:version",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			cmdr: func() command.Commander {
				m := mock_command.NewMockCommander(ctrl)
				m.EXPECT().SigtermReceived().Return(true).Times(2)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				_, err := r.JobScriptReturn("test")
				assert.NoError(t, err, "JobScriptReturn")
				assert.NoError(t, r.JobScriptOutput("test"), "JobScriptReturn")
			},
		},
		"podman script errors": {
			stage: "after_script",
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					RunnerImage: "registry.example.com/runner:version",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			cmdr: func() command.Commander {
				m := mock_command.NewMockCommander(ctrl)
				m.EXPECT().SigtermReceived().Return(false).Times(2)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				_, err := r.JobScriptReturn("")
				assert.Error(t, err, "JobScriptReturn")
				assert.Error(t, r.JobScriptOutput(""), "JobScriptReturn")
			},
		},
		"PrepScript error": {
			stage: "prepare_exec",
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					ApplicationPath: "/usr/bin/podman",
					RunnerImage:     "registry.example.com/runner:version",
					PrepScript:      t.TempDir() + "/missing.bash",
				},
			},
			cmdr: func() command.Commander {
				m := mock_command.NewMockCommander(ctrl)
				m.EXPECT().ReturnOutput(gomock.Any()).Return("script error", errors.New("error message"))
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "failed to run PrepScript")
			},
		},
		"get_sources no image": {
			stage: "get_sources",
			gen: configure.General{
				Executor:     "shell",
				RunMechanism: "podman",
				Podman:       configure.Podman{},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "*basic.mechanism", reflect.TypeOf(r).String())
			},
		},
		"get_sources runner image": {
			stage: "get_sources",
			gen: configure.General{
				Executor:     "shell",
				RunMechanism: "podman",
				Podman: configure.Podman{
					RunnerImage: "registry.example.com/runner:version",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "*podman.mechanism", reflect.TypeOf(r).String())
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.env.StatefulEnv.ScriptDir != "" {
				_ = os.MkdirAll(tt.env.StatefulEnv.ScriptDir, 0700)
			}

			got, err := NewMechanism(
				tt.cmdr,
				tt.env,
				tt.gen,
				tt.msg,
				tt.stage,
			)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertRunner != nil {
				tt.assertRunner(t, got)
			}
			if tt.assertString != nil {
				tt.assertString(t, tt.env.StatefulEnv.ScriptDir)
			}
		})
	}

	t.Run("verify interface functions", func(t *testing.T) {
		ctx := context.TODO()

		cmd := mock_command.NewMockCommander(ctrl)
		cmd.EXPECT().SigtermReceived().Return(false).AnyTimes()
		cmd.EXPECT().PipeOutput(gomock.Any()).Return(nil).Times(2)
		cmd.EXPECT().ReturnOutput(gomock.Any()).Return("/test/job/script", nil).Times(2)
		cmd.EXPECT().RequestContext().Return(ctx).Times(1)
		cmd.EXPECT().CommandDir("/cmd/dir").Times(1)
		cmd.EXPECT().ModifyCmd("/bin/bash", []string{"--noprofile"}).Times(1)

		msg := mock_logging.NewMockMessenger(ctrl)

		got, err := NewMechanism(
			cmd,
			envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir: t.TempDir(),
					ImageName: "registry.example.com/image:latest",
				},
			},
			configure.General{},
			msg,
			"step_script",
		)

		assert.NoError(t, err, "creating NewMechanism")
		assert.NoError(t, got.JobScriptOutput("script"), "JobScriptOutput")
		assert.NoError(t, got.PipeOutput("script"), "PipeOutput")
		assert.False(t, got.SigtermReceived(), "SigtermReceived")
		assert.Equal(t, ctx, got.RequestContext(), "RequestContext")

		got.CommandDir("/cmd/dir")
		got.ModifyCmd("/bin/bash", "--noprofile")

		s, err := got.JobScriptReturn("script", []string{"standard", "in"})
		assert.NoError(t, err, "JobScriptReturn")
		assert.Equal(t, "/test/job/script", s)

		_, err = got.ReturnOutput("script")
		assert.NoError(t, err, "ReturnOutput")
	})
}

func Test_mechanism_TransferScript(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_command.NewMockCommander(ctrl)
	m.EXPECT().AppendEnv([]string{
		"JACAMAR_SCRIPT_CONTENTS_0=IyEvdXNyL2Jpbi9lbnYgYmFzaAoKc2V0IC1lbyBwaXBlZmFpbApzZXQgK28gbm9jbG9iYmVyCjogfCBldmFsICQnZWNobyAiUnVubmluZyBqb2Igc2NyaXB0Li4uIlxuJwpleGl0IDAK",
	}).Times(1)

	tests := map[string]podmanTests{
		"bad file path (src) provided": {
			src:   "/not/a/valid/file.bash",
			stage: t.TempDir() + "/prepare_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: "/var/tmp",
				},
			},
			auth: configure.Auth{
				MaxEnvChars: 1000,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "open /not/a/valid/file.bash: no such file or directory")
			},
		},
		"basic prepare_script provided and transferred to environment": {
			src:   "../../../test/testdata/jobscript.bash",
			stage: t.TempDir() + "/prepare_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: "/var/tmp",
				},
			},
			auth: configure.Auth{
				MaxEnvChars: 1000,
			},
			cmdr: m,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Setenv("TRUSTED_CI_JOB_TOKEN", "T0k3n")

			m := mechanism{
				cmdr: tt.cmdr,
			}
			err := m.TransferScript(tt.src, tt.stage, tt.env, configure.Options{
				Auth: tt.auth,
			})

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
