package podman

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

type authFileContents struct {
	Credentials map[string]authFileCredentials `json:"auths"`
}

type authFileCredentials struct {
	Auth string `json:"auth"`
}

// generateAuthFile creates a default Podman credentials file using the stateful
// registry credentials provided in the GitLab job payload. This is optional,
// no file is created in cases where credentials are missing/undefined.
//
// The resulting file will be structured as:
//
//	{
//	  "auths": {
//		"registry.url": {
//	      "auth":"username:passwordBase64Encoded"
//	    }
//	  }
//	}
//
// The resulting JSON file will be created within the project's job specific
// script directory within the properly protected data_dir. Cleanup of this
// file is handled through existing Jacamar CI mechanism for the script directory.
func generateAuthFile(env envparser.ExecutorEnv) error {
	if env.StatefulEnv.RegistryCredentials == "" {
		return nil
	}

	credentials := strings.Split(env.StatefulEnv.RegistryCredentials, ",")
	contents := authFileContents{
		Credentials: make(map[string]authFileCredentials, len(credentials)),
	}

	for _, cred := range credentials {
		tmp := strings.Split(cred, ":")
		if len(tmp) != 2 {
			return errors.New("invalid registry credentials in stateful environment")
		}
		bURL, _ := base64.RawURLEncoding.DecodeString(tmp[0])
		contents.Credentials[string(bURL)] = authFileCredentials{
			Auth: tmp[1],
		}
	}

	b, err := json.Marshal(&contents)
	if err != nil {
		return err
	}

	return usertools.CreateFile(authFileName(env), string(b))
}

// authFileOpt optionally provides the appropriate flag (--authfile) to be used in
// Podman commands. Specifying if this is support a user defined image is important
// as auth will be handled different with runner images, as user can optionally provide
// their own authfile via CI/CD variables.
func authFileOpt(env envparser.ExecutorEnv, stage string, userImage bool) string {
	if env.StatefulEnv.RegistryCredentials == "" {
		return ""
	}

	contents, found := os.LookupEnv(envkeys.UserEnvPrefix + authFileKey)

	// In this case we will defer to the using the auth file generated using
	// GitLab credentials to whatever the user has supplied.
	if found && userImage {
		var filename string

		_, err := os.Stat(contents)
		if err != nil {
			filename = userAuthFileName(env)

			// There are cases in GitLab where the file will not be created
			// and we will need to generate it first.
			err = usertools.CreateFile(filename, contents)
			if err != nil && stage == "prepare_exec" {
				fmt.Printf(
					"Error encountered generating authfile (%s), this may impact Podman actions\n",
					filename,
				)
			}
		} else {
			filename = contents
		}

		return fmt.Sprintf(" %s '%s'", authOpt, filename)
	}

	return fmt.Sprintf(" %s '%s'", authOpt, authFileName(env))
}

func authFileName(env envparser.ExecutorEnv) string {
	return env.StatefulEnv.ScriptDir + "/auth.json"
}

func userAuthFileName(env envparser.ExecutorEnv) string {
	return env.StatefulEnv.ScriptDir + "/user-auth.json"
}
