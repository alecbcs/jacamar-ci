package basic

import (
	"context"
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_command"
)

// basicTests are as the name of the package implies since there is
// little done to influence the provided job script/commands. However,
// we still need to verify expected functionality at all time.
type basicTests struct {
	src  string
	dest string
	dir  string

	auth    *mock_authuser.MockAuthorized
	cfgAuth configure.Auth
	cmdr    *mock_command.MockCommander
	env     envparser.ExecutorEnv
	msg     logging.Messenger

	assertError  func(*testing.T, error)
	assertRules  func(*testing.T, *augmenter.Rules)
	assertString func(*testing.T, string)
}

func TestNewMechanism(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	stdin := "stdin"
	script := "script"

	mCmdr := mock_command.NewMockCommander(ctrl)
	mCmdr.EXPECT().PipeOutput(stdin + " " + script).Return(nil).Times(1)
	mCmdr.EXPECT().PipeOutput(stdin).Return(nil).Times(1)
	mCmdr.EXPECT().ReturnOutput(stdin+" "+script).Return("out", nil).Times(1)
	mCmdr.EXPECT().ReturnOutput(stdin).Return("out", nil).Times(1)
	mCmdr.EXPECT().CommandDir(stdin).Times(1)
	mCmdr.EXPECT().SigtermReceived().Return(false).AnyTimes()
	mCmdr.EXPECT().PipeOutput(script).Return(nil).Times(1)
	mCmdr.EXPECT().ModifyCmd("/bin/bash", []string{"--noprofile"}).Times(1)

	t.Run("verify no alterations made by basic implementation of runner interface", func(t *testing.T) {
		r := NewMechanism(mCmdr)

		// We are going to ignore returns and just rely on
		// the expectations of the MockCommander.
		_ = r.JobScriptOutput(script, stdin)
		_ = r.JobScriptOutput(script)
		_, _ = r.JobScriptReturn(script, stdin)
		_ = r.PipeOutput(stdin)
		_, _ = r.ReturnOutput(stdin)
		r.CommandDir(stdin)
		r.SigtermReceived()
		r.ModifyCmd("/bin/bash", "--noprofile")
	})

	ctx := context.Background()

	sigCmd := mock_command.NewMockCommander(ctrl)
	sigCmd.EXPECT().SigtermReceived().Return(true).Times(2)
	sigCmd.EXPECT().RequestContext().Return(ctx)

	t.Run("verify SIGTERM prevents future job scripts", func(t *testing.T) {
		r := NewMechanism(sigCmd)

		err := r.JobScriptOutput(script, stdin)
		assert.NoError(t, err)

		_, err = r.JobScriptReturn(script, stdin)
		assert.NoError(t, err)

		got := r.RequestContext()
		assert.Equal(t, ctx, got)
	})
}

func Test_mechanism_TransferScript(t *testing.T) {
	_ = os.Setenv("TRUSTED_CI_JOB_TOKEN", "T0k3n")
	defer func() { _ = os.Unsetenv("TRUSTED_CI_JOB_TOKEN") }()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_command.NewMockCommander(ctrl)
	m.EXPECT().AppendEnv([]string{
		"JACAMAR_SCRIPT_CONTENTS_0=IyEvdXNyL2Jpbi9lbnYgYmFzaAoKc2V0IC1lbyBwaXBlZmFpbApzZXQgK28gbm9jbG9iYmVyCjogfCBldmFsICQnZWNobyAiUnVubmluZyBqb2Igc2NyaXB0Li4uIlxuJwpleGl0IDAK",
	}).Times(1)

	tests := map[string]basicTests{
		"bad file path (src) provided": {
			src:  "/not/a/valid/file.bash",
			dest: "prepare_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: "/var/tmp",
				},
			},
			cfgAuth: configure.Auth{
				MaxEnvChars: 1000,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "open /not/a/valid/file.bash: no such file or directory")
			},
		},
		"basic prepare_script provided and transferred to environment": {
			src:  "../../../test/testdata/jobscript.bash",
			dest: "prepare_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: "/var/tmp",
				},
			},
			cfgAuth: configure.Auth{
				MaxEnvChars: 1000,
			},
			cmdr: m,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			m := mechanism{
				cmdr: tt.cmdr,
			}
			err := m.TransferScript(tt.src, tt.dest, tt.env, configure.Options{
				Auth: tt.cfgAuth,
			})

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
