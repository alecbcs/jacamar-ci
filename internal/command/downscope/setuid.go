package downscope

import (
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"syscall"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

// CreateSetuidShell generates a setuid enabled userspace command that uses the authorized
// user to execute the jacamar application (e.g., /opt/jacamar/bin/jacamar --no-auth ...).
// All commands executed through this interface will be through the user scoped jacamar
// application with a strictly controlled environment. It is expected that this is invoked
// and utilized as part of a jacamar-auth application when the "setuid" downscope is specified.
func (f Factory) CreateSetuidShell() *shell {
	s := &shell{
		abs: f.AbsCmdr,
		cmd: nil,
	}

	name, args := command.JacamarCmd(f.Cfg.Auth(), f.Concrete)
	s.build(name, args...)

	s.targetValidUser(f.AuthUser.CIUser())
	s.cmd.Env = safeEnv(f.AuthUser, f.Cfg.Auth())
	s.CommandDir(f.identifyWorkDir())

	// Build signal interface using current command as a base.
	s.sig = f.createSetuidSignals(s.CopiedCmd())

	f.SysLog.Info("setuid command target account: ", f.AuthUser.CIUser().Username)
	f.SysLog.Debug("downscope command generated: ", strings.Join(s.cmd.Args, " "))

	return s
}

func (s *shell) targetValidUser(usrCtx authuser.UserContext) {
	s.cmd.SysProcAttr.Credential = &syscall.Credential{
		Uid:         uint32(usrCtx.UID),
		Gid:         uint32(usrCtx.GID),
		Groups:      usrCtx.Groups,
		NoSetGroups: false,
	}
}

func (f Factory) createSetuidSignals(baseCmd *exec.Cmd) command.Signaler {
	if signalsAllowed() {
		// Clearly log cases where standard signals may be used.
		f.SysLog.Info("system signals allowed")

		return command.SystemSignals{
			OptionalLog: f.SysLog,
		}
	}

	return &setuidSignals{
		baseCmd: baseCmd,
		sysLog:  f.SysLog,
		auth:    f.Cfg.Auth(),
	}
}

type setuidSignals struct {
	// baseCmd expected duplicate of validate setuid downscope command that
	// can be modified as required to account for signal requirements.
	baseCmd *exec.Cmd
	sysLog  *logrus.Entry
	auth    configure.Auth
	mutex   sync.Mutex
}

func (sys *setuidSignals) SIGTERM(pid int) error {
	return sys.execSignal("SIGTERM", strconv.Itoa(pid))
}

func (sys *setuidSignals) SIGKILL(pid int) error {
	return sys.execSignal("SIGKILL", strconv.Itoa(pid))
}

func (sys *setuidSignals) execSignal(signal, pid string) error {
	sys.mutex.Lock()
	// Avoid attempting to create cmd or send signal while waiting
	// on a similarly existing command to finish.
	defer sys.mutex.Unlock()

	cmd := &exec.Cmd{}
	command.CloneCmd(sys.baseCmd, cmd)

	name, args := command.JacamarCmd(sys.auth, arguments.ConcreteArgs{
		Signal: &arguments.SignalCmd{
			Signal: signal,
			PID:    pid,
		},
	})
	modifySetuid(cmd, name, args)
	sys.sysLog.Debug("prepared setuid signal cmd: ", strings.Join(cmd.Args, " "))

	return runSuppressOutput(cmd)
}

func modifySetuid(cmd *exec.Cmd, name string, args []string) {
	cmd.Path = name
	cmd.Args = append([]string{name}, args...)
}
