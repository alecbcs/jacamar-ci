package downscope

import (
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
)

// CreateSudoShell generates a sudo enabled userspace command that uses the authorized
// user to execute the jacamar application (e.g., /opt/jacamar/bin/jacamar --no-auth ...).
// All commands executed through this interface will be through the user scoped jacamar
// application with a strictly controlled environment. It is expected that this is invoked
// and utilized as part of a jacamar-auth application when the "sudo" downscope is specified.
func (f Factory) CreateSudoShell() *shell {
	// Build Sudo command with specific behaviors based upon manuals:
	// sudo(8): https://man7.org/linux/man-pages/man8/sudo.8.html
	// -E (preserve-env): Keep custom executor established environment
	// -u {username}: Target for downscope based upon authorization
	args := []string{"-E", "-u", f.AuthUser.CIUser().Username, "--"}

	f.SysLog.Info("sudo command target account: ", f.AuthUser.CIUser().Username)

	return f.buildSudo("sudo", args)
}

func (f Factory) buildSudo(name string, arg []string) *shell {
	s := &shell{
		abs: f.AbsCmdr,
		sig: command.SystemSignals{
			OptionalLog: f.SysLog,
		},
		cmd: nil,
		pty: true,
	}

	jName, jArgs := command.JacamarCmd(f.Cfg.Auth(), f.Concrete)
	arg = append(arg, jName)
	arg = append(arg, jArgs...)

	s.build(name, arg...)
	s.cmd.Env = safeEnv(f.AuthUser, f.Cfg.Auth())
	s.CommandDir(f.identifyWorkDir())

	f.SysLog.Debugf("downscope command generated: %s", strings.Join(s.cmd.Args, " "))

	return s
}
