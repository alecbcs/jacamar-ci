package downscope

import (
	"fmt"
	"os/exec"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

// CreateSuShell generates a su enabled userspace command that uses the authorized
// user to execute the jacamar application (e.g., /opt/jacamar/bin/jacamar --no-auth ...).
// All commands executed through this interface will be through the user scoped jacamar
// application with a strictly controlled environment. It is expected that this is invoked
// and utilized as part of a jacamar-auth application when the "su" downscope is specified.
func (f Factory) CreateSuShell() *shell {
	f.SysLog.Info("su command target account: ", f.AuthUser.CIUser().Username)

	return f.buildSu("su", suArguments(f.Cfg.General(), f.AuthUser.CIUser().Username))
}

func (f Factory) buildSu(name string, arg []string) *shell {
	s := &shell{
		abs: f.AbsCmdr,
		sig: command.SystemSignals{},
		cmd: nil,
		pty: true,
	}

	jName, jArgs := command.JacamarCmd(f.Cfg.Auth(), f.Concrete)
	jCmd := fmt.Sprintf("%s %s", jName, strings.Join(jArgs, " "))
	arg = append(arg, jCmd)

	s.build(name, arg...)
	s.cmd.Env = safeEnv(f.AuthUser, f.Cfg.Auth())
	s.CommandDir(f.identifyWorkDir())

	f.SysLog.Debug("downscope command generated: ", strings.Join(s.cmd.Args, " "))

	return s
}

func suArguments(gen configure.General, username string) []string {
	argumentList := []string{
		username,
		"-m",
		"-s",
		command.IdentifyShell(gen),
	}
	// CENTOS doesn't have the PTY flag for the su command.
	// Proactively check for it before appending it to the list.

	if cmd, _ := exec.Command("su", "--help").Output(); strings.Contains(string(cmd), "--pty") {
		argumentList = append(argumentList, "--pty")
	}
	argumentList = append(argumentList, "-c")
	return argumentList
}
