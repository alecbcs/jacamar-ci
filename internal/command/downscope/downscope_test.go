package downscope

import (
	"context"
	"io"
	"os/exec"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type downTests struct {
	script    string
	targetEnv map[string]string

	s       *shell
	cfg     configure.Configurer
	absCmdr *command.AbstractCommander
	auth    *mock_authuser.MockAuthorized

	mockSignalsAllowed func() bool

	assertError  func(*testing.T, error)
	assertShell  func(*testing.T, *shell)
	assertString func(*testing.T, string)
}

// testSysLog is an empty logger, syslog tested via Pavilion2 primarily.
var testSysLog *logrus.Entry

func init() {
	ll := logrus.New()
	ll.Out = io.Discard
	testSysLog = logrus.NewEntry(ll)
}

func mockGenConfig(ctrl *gomock.Controller, auth configure.Auth, gen configure.General) *mock_configure.MockConfigurer {
	m := mock_configure.NewMockConfigurer(ctrl)
	m.EXPECT().General().Return(gen).AnyTimes()
	m.EXPECT().Auth().Return(auth).AnyTimes()
	return m
}

func mockAuthWorkingID(ctrl *gomock.Controller) *mock_authuser.MockAuthorized {
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().CIUser().Return(authuser.UserContext{
		Username: "user",
		HomeDir:  "/home/user",
		UID:      1001,
		GID:      2002,
		Groups:   []uint32{1001, 42},
	}).AnyTimes()
	return m
}

func Test_shell_PipeOutput(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	cfg := mockGenConfig(
		ctrl,
		configure.Auth{},
		configure.General{
			KillTimeout: "5s",
		},
	)

	bash := &shell{
		abs: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
		cmd: nil,
	}
	bash.build("/bin/bash")

	pty := &shell{
		abs: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
		cmd: nil,
		pty: true,
	}
	pty.build("/bin/bash", "-c")

	tests := map[string]downTests{
		"exit 0 command, successfully executed": {
			s:      bash,
			script: `echo "hello" && exit 0`,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"exit 1 command, successfully executed": {
			s:      bash,
			script: `echo "world" && exit 1`,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"pseudo terminal in use": {
			s:      pty,
			script: `date`,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.s.PipeOutput(tt.script)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_shell_ReturnOutput(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	cfg := mockGenConfig(
		ctrl,
		configure.Auth{},
		configure.General{
			KillTimeout: "5s",
		},
	)

	bash := &shell{
		abs: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
		cmd: nil,
	}
	bash.build("/bin/bash")

	pty := &shell{
		abs: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
		cmd: nil,
		pty: true,
	}
	pty.build("/bin/bash", "-c")

	tests := map[string]downTests{
		"exit 0 command, successfully executed": {
			s:      bash,
			script: `echo "hello" && exit 0`,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "hello")
			},
		},
		"exit 1 command, successfully executed": {
			s:      bash,
			script: `echo "world" && exit 1`,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "world")
			},
		},
		"pseudo terminal in use": {
			s:      pty,
			script: `echo "TEST"`,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "TEST")
			},
		},
		"output limited": {
			s: bash,
			script: func() string {
				b := make([]byte, byteLimit*4)
				for i := range b {
					b[i] = "a"[0]
				}
				return "echo " + string(b)
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, byteLimit, len(s))
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := tt.s.ReturnOutput(tt.script)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func Test_MiscInterface(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	cfg := mockGenConfig(
		ctrl,
		configure.Auth{},
		configure.General{
			KillTimeout: "5s",
		},
	)

	s := &shell{
		abs: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
		cmd: nil,
	}
	s.build("/usr/bin/env", "-i", "--login")

	t.Run("updated command directory", func(t *testing.T) {
		s.CommandDir("/new")
		assert.Equal(t, s.cmd.Dir, "/new")
	})
	t.Run("append env", func(t *testing.T) {
		s.AppendEnv([]string{"HELLO=WORLD"})
		assert.Contains(t, s.cmd.Env, "HELLO=WORLD")
	})
	t.Run("check sigterm received", func(t *testing.T) {
		got := s.SigtermReceived()
		assert.False(t, got)
	})
	t.Run("request context", func(t *testing.T) {
		got := s.RequestContext()
		assert.Nil(t, got.Err())
	})
	t.Run("request copied command", func(t *testing.T) {
		got := s.CopiedCmd()
		assert.NotNil(t, got)
		assert.Equal(t, "/usr/bin/env", got.Path)
	})
	t.Run("command not modified", func(t *testing.T) {
		got := s.CopiedCmd()
		s.ModifyCmd("date")
		assert.NotNil(t, s.cmd)
		assert.Equal(t, got, s.cmd)
	})
	t.Run("successful command, no output", func(t *testing.T) {
		cmd := exec.Command("date")
		err := runSuppressOutput(cmd)
		assert.NoError(t, err)
	})
}

func TestFactory_identifyWorkDir(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	noTar := mock_configure.NewMockConfigurer(ctrl)
	noTar.EXPECT().Auth().Return(configure.Auth{}).AnyTimes()

	target := mock_configure.NewMockConfigurer(ctrl)
	target.EXPECT().Auth().Return(configure.Auth{
		DownscopeCmdDir: "/example/bin",
	}).AnyTimes()

	tests := []struct {
		name         string
		factory      Factory
		mockPath     func()
		assertString func(*testing.T, string)
	}{
		{
			name: "default path used",
			factory: Factory{
				Cfg: noTar,
				Concrete: arguments.ConcreteArgs{
					Prepare: &arguments.PrepareCmd{},
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.NotEmpty(t, s)
			},
		}, {
			name: "configured path observed",
			factory: Factory{
				Cfg: target,
				Concrete: arguments.ConcreteArgs{
					Prepare: &arguments.PrepareCmd{},
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "/example/bin", s)
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.factory.identifyWorkDir()

			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func Test_safeEnv(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().CIUser().Return(authuser.UserContext{
		Username: "user",
		HomeDir:  "/home/user",
	}).AnyTimes()

	tests := map[string]struct {
		cfg       configure.Auth
		auth      *mock_authuser.MockAuthorized
		targetEnv map[string]string
		assertEnv func(*testing.T, []string)
	}{
		"standard \"safe\" environment": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "A":          "foo",
				envkeys.StatefulEnvPrefix + "B":      "bar",
				envkeys.UserEnvPrefix + "CI_JOB_JWT": "T0k3n",
			},
			auth: m,
			assertEnv: func(t *testing.T, e []string) {
				assert.Contains(t, e, "HOME=/home/user")
				assert.Contains(t, e, envkeys.UserEnvPrefix+"A"+"=foo")
				assert.Contains(t, e, envkeys.StatefulEnvPrefix+"B"+"=bar")
				assert.Contains(t, e, "PATH=/usr/bin:/bin:/usr/local/bin:/usr/local/sbin:/usr/sbin")
				assert.NotContains(t, e, "Tok3n")
			},
		},
		"included downscope_env configuration": {
			targetEnv: map[string]string{},
			auth:      m,
			cfg: configure.Auth{
				DownscopeEnv: []string{
					"HELLO=WORLD",
					"TESTING=123",
				},
			},
			assertEnv: func(t *testing.T, e []string) {
				assert.Contains(t, e, "HOME=/home/user")
				assert.Contains(t, e, "HELLO=WORLD")
				assert.Contains(t, e, "TESTING=123")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for key, value := range tt.targetEnv {
				t.Setenv(key, value)
			}

			got := safeEnv(tt.auth, tt.cfg)
			tt.assertEnv(t, got)
		})
	}
}
