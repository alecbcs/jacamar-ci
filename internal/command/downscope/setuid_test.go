package downscope

import (
	"context"
	"os/exec"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/verifycaps"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func TestFactory_CreateSetuidShell(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	t.Setenv("CREATE_SETUID_SHELL", "TEST")

	m := mock_logging.NewMockMessenger(ctrl)

	cfg := mockGenConfig(
		ctrl,
		configure.Auth{
			JacamarPath: "/bin",
		},
		configure.General{
			Shell:       "/bin/bash",
			KillTimeout: "5s",
		},
	)

	cmdDir := mockGenConfig(
		ctrl,
		configure.Auth{
			DownscopeCmdDir: "/var/tmp",
			JacamarPath:     "/bin",
		},
		configure.General{
			Shell:       "/bin/bash",
			KillTimeout: "5s",
		},
	)

	tests := map[string]downTests{
		"valid setuid bash command generated": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
			cfg:     cfg,
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				assert.Equal(t, "/bin/jacamar", s.cmd.Path)
				assert.Equal(t, uint32(1001), s.cmd.SysProcAttr.Credential.Uid)
				assert.Equal(t, "/bin", s.cmd.Dir)
			},
		},
		"SigtermReceived functions correctly against defaults": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
			cfg:     cfg,
			assertShell: func(t *testing.T, s *shell) {
				assert.False(t, s.SigtermReceived(), "default SigtermReceived must be false")
			},
		},
		"underlying command AppendEnv properly function": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
			cfg:     cfg,
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s)
				s.AppendEnv([]string{"FOO=BAR"})
				assert.Contains(t, s.cmd.Env, "FOO=BAR")
			},
		},
		"RequestContext returns context pointer": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
			cfg:     cfg,
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.RequestContext())
			},
		},
		"setuid shell create with safe environment": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
			cfg:     cfg,
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				assert.NotContains(t, s.cmd.Env, "CREATE_SETUID_SHELL=TEST")
			},
		},
		"setuid shell created with configured command directory": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cmdDir.General(), m, envparser.ExecutorEnv{}, testSysLog),
			cfg:     cmdDir,
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				assert.Equal(t, s.cmd.Dir, "/var/tmp")
			},
		},
		"root user signal management": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
			mockSignalsAllowed: func() bool {
				return true
			},
			cfg: cfg,
			assertShell: func(t *testing.T, s *shell) {
				assert.IsType(t, command.SystemSignals{}, s.sig)
			},
		},
		"non-root user signal management": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
			cfg:     cfg,
			mockSignalsAllowed: func() bool {
				return false
			},
			assertShell: func(t *testing.T, s *shell) {
				assert.IsType(t, &setuidSignals{}, s.sig)
				assert.NotNil(t, s.sig.(*setuidSignals).baseCmd)
				assert.NotNil(t, s.sig.(*setuidSignals).sysLog)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockSignalsAllowed != nil {
				signalsAllowed = tt.mockSignalsAllowed
			} else {
				signalsAllowed = verifycaps.SignalAllowed
			}

			f := Factory{
				AbsCmdr:  tt.absCmdr,
				SysLog:   testSysLog,
				Cfg:      tt.cfg,
				AuthUser: tt.auth,
			}
			s := f.CreateSetuidShell()

			if tt.assertShell != nil {
				tt.assertShell(t, s)
			}
		})
	}
}

func TestMechanism_targetValidUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	workingShell := &shell{}
	workingShell.build("bash")

	tests := map[string]downTests{
		"valid command generated": {
			auth: mockAuthWorkingID(ctrl),
			s:    workingShell,
			assertShell: func(t *testing.T, s *shell) {
				cmd := s.cmd
				assert.NotNil(t, cmd)
				assert.True(t, strings.HasSuffix(cmd.Path, "bash"))
				assert.Equal(t, uint32(1001), cmd.SysProcAttr.Credential.Uid, "target UID")
				assert.Equal(t, uint32(2002), cmd.SysProcAttr.Credential.Gid, "target GID")
				assert.Equal(t, []uint32{1001, 42}, cmd.SysProcAttr.Credential.Groups)
				assert.Equal(t, false, cmd.SysProcAttr.Credential.NoSetGroups)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			tt.s.targetValidUser(tt.auth.CIUser())

			if tt.assertShell != nil {
				tt.assertShell(t, tt.s)
			}
		})
	}
}

func Test_setuidSignals(t *testing.T) {
	baseCmd := exec.Command("test", []string{"args"}...)

	// We can't reasonably test signal with unit tests, instead
	// verify encountered errors are returned.

	t.Run("run SIGTERM, capture error", func(t *testing.T) {
		sys := &setuidSignals{
			baseCmd: baseCmd,
			sysLog:  testSysLog,
			auth: configure.Auth{
				JacamarPath: "/bin",
			},
		}

		err := sys.SIGTERM(999999)
		assert.EqualError(t, err, "fork/exec /bin/jacamar: no such file or directory")
	})
	t.Run("run SIGKILL, capture error", func(t *testing.T) {
		sys := &setuidSignals{
			baseCmd: baseCmd,
			sysLog:  testSysLog,
			auth: configure.Auth{
				JacamarPath: "/bin",
			},
		}

		err := sys.SIGKILL(999999)
		assert.EqualError(t, err, "fork/exec /bin/jacamar: no such file or directory")
	})
}
