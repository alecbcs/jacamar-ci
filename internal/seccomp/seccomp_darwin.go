// Package seccomp for managing Jacamar's bindings for libseccomp, raises error on Darwin.
// This is to help avoid errors while developing on a Mac.
package seccomp

import (
	"errors"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

type Factory struct {
	Stage  string
	Opt    configure.Options
	Usr    authuser.UserContext
	SysLog *logrus.Entry
}

// Establish will fail on Darwin.
func (f Factory) Establish() error {
	return errors.New("seccomp not supported on MacOS")
}

func SetNoNewPrivs() error {
	return nil
}
