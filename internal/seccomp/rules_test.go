//go:build linux
// +build linux

package seccomp

import (
	"syscall"
	"testing"

	libseccomp "github.com/seccomp/libseccomp-golang"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

func TestFactory_generate(t *testing.T) {
	tests := map[string]struct {
		f         Factory
		assertCfg func(*testing.T, config)
	}{
		"downscope setuid rules": {
			f: Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
						Seccomp: configure.Seccomp{
							LimitSetuid: true,
							TTYRules:    false,
						},
					},
				},
				Usr: authuser.UserContext{
					UID: 1000,
					GID: 2000,
				},
			},
			assertCfg: func(t *testing.T, c config) {
				assert.Equal(t, defaultAllowAct, c.defaultAction)
				assert.Len(t, c.rules, 2)

				// setuid + setgid rules
				assert.Contains(t, c.rules, rule{"setuid", defaultBlockAct, []condition{condition{libseccomp.CompareNotEqual, 0, uint64(1000)}}})
				assert.Contains(t, c.rules, rule{"setgid", defaultBlockAct, []condition{condition{libseccomp.CompareNotEqual, 0, uint64(2000)}}})
			},
		},
		"downscope no setuid rules": {
			f: Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
						Seccomp: configure.Seccomp{
							LimitSetuid: false,
						},
					},
				},
				Usr: authuser.UserContext{
					UID: 1000,
					GID: 2000,
				},
			},
			assertCfg: func(t *testing.T, c config) {
				assert.NotContains(t, c.rules, rule{"setuid", defaultBlockAct, []condition{condition{libseccomp.CompareNotEqual, 0, uint64(1000)}}})
				assert.NotContains(t, c.rules, rule{"setgid", defaultBlockAct, []condition{condition{libseccomp.CompareNotEqual, 0, uint64(2000)}}})
			},
		},
		"block all by default": {
			f: Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						Seccomp: configure.Seccomp{
							BlockAll:   true,
							AllowCalls: []string{"mkdir", "setuid"},
						},
					},
				},
			},
			assertCfg: func(t *testing.T, c config) {
				assert.Equal(t, defaultBlockAct, c.defaultAction)

				assert.Equal(t, c.adminRules["mkdir"], defaultAllowAct)
				assert.Equal(t, c.adminRules["setuid"], defaultAllowAct)
			},
		},
		"skip ioctl when already blocked": {
			f: Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
						Seccomp: configure.Seccomp{
							TTYRules:   true,
							BlockCalls: []string{"ioctl"},
						},
					},
				},
			},
			assertCfg: func(t *testing.T, c config) {
				assert.NotContains(t, c.rules, rule{"ioctl", defaultBlockAct, []condition{condition{libseccomp.CompareEqual, 1, syscall.TIOCSTI}}})
			},
		},
		"observer admin block rules": {
			f: Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "sudo",
						Seccomp: configure.Seccomp{
							BlockCalls: []string{"mkdir"},
						},
					},
				},
			},
			assertCfg: func(t *testing.T, c config) {
				assert.Equal(t, c.adminRules["mkdir"], defaultBlockAct)
			},
		},
		"plugin feature flag observed": {
			f: Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						Seccomp: configure.Seccomp{
							FilterPlugin: "/opt/jacamar/plugin/seccomp.so",
						},
					},
				},
			},
			assertCfg: func(t *testing.T, c config) {
				assert.Equal(t, "/opt/jacamar/plugin/seccomp.so", c.pluginFile)
			},
		},
		"downscope tty rules": {
			f: Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
						Seccomp: configure.Seccomp{
							TTYRules: true,
						},
					},
				},
				Usr: authuser.UserContext{
					UID: 1000,
					GID: 2000,
				},
			},
			assertCfg: func(t *testing.T, c config) {
				assert.Equal(t, defaultAllowAct, c.defaultAction)
				assert.Len(t, c.rules, 1)

				assert.Contains(t, c.rules, rule{"ioctl", defaultBlockAct, []condition{condition{libseccomp.CompareEqual, 1, syscall.TIOCSTI}}})
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := tt.f.generate()

			if tt.assertCfg != nil {
				tt.assertCfg(t, got)
			}
		})
	}
}

func TestFactory_setActions(t *testing.T) {
	tests := map[string]struct {
		f             Factory
		apiLvl        uint
		assertActions func(*testing.T, libseccomp.ScmpAction, libseccomp.ScmpAction, libseccomp.ScmpAction)
	}{
		"log_allowed_actions not possible": {
			f: Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						Seccomp: configure.Seccomp{
							LogAllowedActions: true,
						},
					},
				},
				SysLog: sysLog,
			},
			apiLvl: uint(1),
			assertActions: func(t *testing.T, d libseccomp.ScmpAction, a libseccomp.ScmpAction, b libseccomp.ScmpAction) {
				assert.Equal(t, d, libseccomp.ActAllow)
				assert.Equal(t, a, defaultAllowAct)
				assert.Equal(t, b, defaultBlockAct)
			},
		},
		"log_allowed_actions": {
			f: Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						Seccomp: configure.Seccomp{
							LogAllowedActions: true,
						},
					},
				},
				SysLog: sysLog,
			},
			apiLvl: uint(3),
			assertActions: func(t *testing.T, d libseccomp.ScmpAction, a libseccomp.ScmpAction, b libseccomp.ScmpAction) {
				assert.Equal(t, d, libseccomp.ActLog)
				assert.Equal(t, a, libseccomp.ActLog)
				assert.Equal(t, b, defaultBlockAct)
			},
		},
		"error_num_block_actions": {
			f: Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						Seccomp: configure.Seccomp{
							ErrorNumBlockActions: true,
						},
					},
				},
				SysLog: sysLog,
			},
			apiLvl: uint(3),
			assertActions: func(t *testing.T, d libseccomp.ScmpAction, a libseccomp.ScmpAction, b libseccomp.ScmpAction) {
				assert.Equal(t, b, libseccomp.ActErrno.SetReturnCode(int16(syscall.EPERM)))
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			d, a, b := tt.f.setActions(tt.apiLvl)

			if tt.assertActions != nil {
				tt.assertActions(t, d, a, b)
			}
		})
	}
}
