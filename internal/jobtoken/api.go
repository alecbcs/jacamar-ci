package jobtoken

import (
	"net/url"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/gitwebapis"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

const (
	// https://docs.gitlab.com/ee/api/jobs.html#get-job-tokens-job
	jobPath = "/api/v4/job"
)

type JobResponse struct {
	Name string `json:"name"`
	User struct {
		ID       int    `json:"id"`
		Username string `json:"username"`
		// Missing user email
	} `json:"user"`
	Project struct {
		JobTokenScopeEnabled bool `json:"ci_job_token_scope_enabled"`
	} `json:"project"`
}

func getJob(api gitwebapis.Client, req envparser.RequiredEnv) (resp JobResponse, err error) {
	urlStr, err := jobURL(req)
	if err != nil {
		// In all likelihood this is difficult to encounter and an early step should fail
		// on an invalid URL before reaching this point.
		return
	}

	err = api.GetJSON(urlStr, map[string]string{"JOB-TOKEN": req.JobToken}, &resp)

	return
}

func jobURL(req envparser.RequiredEnv) (string, error) {
	u, err := url.Parse(req.ServerURL)
	if err != nil {
		return "", err
	}

	u.Path = strings.TrimRightFunc(u.Path, func(r rune) bool {
		return r == '/'
	}) + jobPath

	return u.String(), nil
}
