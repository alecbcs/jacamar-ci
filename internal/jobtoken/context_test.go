package jobtoken

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

type jwtCtxTests struct {
	stage string
	opt   configure.Options
	env   envparser.ExecutorEnv

	assertValidator func(*testing.T, Validator)
	assertJob       func(*testing.T, *JobJWT)
}

func TestJwtInterface(t *testing.T) {
	t.Run("basic interface", func(t *testing.T) {
		j := &JobJWT{
			claims: gljobctx.Claims{
				PipelineSource: "source",
				ProjectID:      "123",
				ProjectPath:    "group/project",
				UserLogin:      "username",
			},
			req:               envparser.RequiredEnv{},
			enforceTokenScope: true,
		}

		assert.Error(t, j.EnforceTokenScope())

		assert.Equal(t, "123", j.ProjectID())
		assert.Equal(t, "group/project", j.ProjectPath())
		assert.Equal(t, "source", j.PipelineSource())
		assert.Equal(t, "username", j.UserLogin())

		assert.NotEmpty(t, j.DebugMsg())
		assert.NotEmpty(t, j.Environment())
	})
}

func TestNewJob(t *testing.T) {
	tests := map[string]jwtCtxTests{
		"new arbitrary JWT": {
			stage: "prepare_exec",
			assertValidator: func(t *testing.T, validator Validator) {
				assert.NotNil(t, validator)
				if validator != nil {
					_, err := validator.Run()
					assert.Error(t, err)
				}
			},
		},
		"delayed not observed": {
			stage: "prepare_exec",
			opt: configure.Options{
				Auth: configure.Auth{
					JWTExpDelay: "invalid",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					Username: "username",
				},
			},
			assertValidator: func(t *testing.T, validator Validator) {
				assert.NotNil(t, validator)
				if validator != nil {
					_, err := validator.Run()
					assert.Error(t, err)
					if err != nil {
						assert.Contains(t, err.Error(), "unable to parse supplied id_token")
					}
				}
			},
		},
		"invalid expiration delay": {
			stage: "cleanup_exec",
			opt: configure.Options{
				Auth: configure.Auth{
					JWTExpDelay: "invalid",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					Username: "username",
				},
			},
			assertValidator: func(t *testing.T, validator Validator) {
				assert.NotNil(t, validator)
				if validator != nil {
					_, err := validator.Run()
					assert.EqualError(t, err, "time: invalid duration \"invalid\"")
				}
			},
		},
		"jwks error": {
			stage: "config_exec",
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID:     "123",
					ServerURL: "https://127.0.0.1",
					CIJobJWT:  "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6InNsdWcvdGVzdCIsImpvYl9pZCI6MTIzLCJleHAiOjMyNDkxODIwMTYwfQ.GCZ02BpMdbUkXrXCd1zBZWWco7p8QMPXjRdxCb9ICGeGxxtxF4eUeYgjz-5XFLyLUuL3JbxuT5atm8ckMR_DUr6Ami6knDGFvWUjHD007RT9lUghBdqDOp--2eTNRbI4ivwU87oC-sfKx9F9X9CTTN85fLQesx6STm7GQRzHMFJCmMN2p0fdxAZMQcmoSJioe7YYLxPvNyu4ylHYZHA6WJ0FzEwUj23R1Cwm74H59b4O47DSILH8T49lEJRvp_WGYkFPhREnldYcQtWX0BJHL-hrpYfUy6WjQdviPj_X7jnKmsG5madX_dkvWDRRQ_0SLfeqiQlG6pqpAEjjMGlnoA",
				},
			},
			assertValidator: func(t *testing.T, validator Validator) {
				assert.NotNil(t, validator)
				if validator != nil {
					_, err := validator.Run()
					assert.Error(t, err)
					if err != nil {
						assert.Contains(t, err.Error(), "unable to parse supplied id_token")
					}
				}
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := NewJob(tt.stage, tt.opt, tt.env)

			if tt.assertValidator != nil {
				tt.assertValidator(t, got)
			}
		})
	}
}
