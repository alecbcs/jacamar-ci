package jobtoken

import (
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/gitwebapis"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_gitwebapis"
)

func Test_Factory_checkTokenScope(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]struct {
		api gitwebapis.Client
		req envparser.RequiredEnv
		msg logging.Messenger

		assertErr func(*testing.T, error)
	}{
		"invalid parsed server URL": {
			req: envparser.RequiredEnv{
				ServerURL: "https:// invalid.example.com",
			},
			assertErr: func(t *testing.T, err error) {
				assert.EqualError(t, err, "parse \"https:// invalid.example.com\": invalid character \" \" in host name")
			},
		},
		"scope disabled": {
			api: func() gitwebapis.Client {
				m := mock_gitwebapis.NewMockClient(ctrl)
				m.EXPECT().GetJSON(
					"https://gitlab.example.com/api/v4/job",
					map[string]string{"JOB-TOKEN": "disabled"},
					gomock.Any(),
				).Return(nil)
				return m
			}(),
			req: envparser.RequiredEnv{
				JobToken:  "disabled",
				ServerURL: "https://gitlab.example.com",
			},
			msg: logging.NewMessenger(),
			assertErr: func(t *testing.T, err error) {
				assert.EqualError(t, err, "ci_job_token_scope_enabled not enabled for the project")
			},
		},
		"scope disabled trimRight": {
			api: func() gitwebapis.Client {
				m := mock_gitwebapis.NewMockClient(ctrl)
				m.EXPECT().GetJSON(
					"https://gitlab.example.com/api/v4/job",
					map[string]string{"JOB-TOKEN": "enabled"},
					gomock.Any(),
				).Return(nil)
				return m
			}(),
			req: envparser.RequiredEnv{
				JobToken:  "enabled",
				ServerURL: "https://gitlab.example.com/",
			},
			msg: logging.NewMessenger(),
			assertErr: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			j := &JobJWT{
				req: tt.req,
			}
			err := j.checkTokenScope(tt.api, tt.msg)

			if tt.assertErr != nil {
				tt.assertErr(t, err)
			}
		})
	}
}

func Test_Factory_checkTokenScope_gitlab(t *testing.T) {
	server := os.Getenv("CI_SERVER_URL")
	jobToken := os.Getenv("CI_JOB_TOKEN")

	if jobToken == "" || server == "" {
		t.Skip("required CI env not found")
	}

	projectID := os.Getenv("CI_PROJECT_ID")
	if projectID != "13829536" {
		// Required feature can only be found on gitlab.com currently, limit to our project.
		t.Skip("only required for ecp-ci/jacamar-ci at this time")
	}

	t.Run("test scope during GitLab CI", func(t *testing.T) {
		j := &JobJWT{
			req: envparser.RequiredEnv{
				ServerURL: server,
				JobToken:  jobToken,
			},
		}
		err := j.checkTokenScope(gitwebapis.DefaultClient(), logging.NewMessenger())
		assert.NoError(t, err)
	})
}
