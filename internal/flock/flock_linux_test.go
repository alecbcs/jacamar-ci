package flock

import (
	"os"
	"sync"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func TestManageLock(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	sysErr := func() { return }
	exitZero := func() { return }

	tests := map[string]struct {
		msg  logging.Messenger
		lCmd arguments.LockCmd
	}{
		"invalid timeout error observed": {
			lCmd: arguments.LockCmd{
				JobTimeout: "abc",
			},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Stderr("Invalid timeout: %s", "abc").Times(1)
				m.EXPECT().Stderr(gomock.Any(), gomock.Any()).AnyTimes()
				m.EXPECT().Stdout(gomock.Any()).AnyTimes()
				return m
			}(),
		},
		"invalid proposedDir error observed": {
			lCmd: arguments.LockCmd{
				JobTimeout: "1000",
			},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Stderr("Unable to generate lock: %s", gomock.Any()).Times(1)
				m.EXPECT().Stdout(gomock.Any()).AnyTimes()
				return m
			}(),
		},
		"valid workflow": {
			lCmd: arguments.LockCmd{
				JobID:       "123",
				JobTimeout:  "1000",
				ProposedDir: t.TempDir(),
			},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Stdout("000").AnyTimes()
				return m
			}(),
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			ManageLock(tt.msg, tt.lCmd, sysErr, exitZero)
		})
	}
}

func TestGenerate(t *testing.T) {
	tests := map[string]struct {
		proposedDir string
		jobID       string
		jobTimeout  int
		mode        os.FileMode

		assertString func(*testing.T, string, string)
		assertError  func(*testing.T, error)
	}{
		"single generate process": {
			proposedDir: t.TempDir() + "/single",
			jobID:       "123",
			jobTimeout:  3600,
			mode:        0700,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s, dir string) {
				assert.Equal(t, "000", s)
				_, err := os.ReadDir(dir + "/000")
				assert.NoError(t, err, "missing directory")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := generate(tt.proposedDir, tt.jobID, tt.jobTimeout, tt.mode)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, got, tt.proposedDir)
			}
		})
	}

	t.Run("multiple generate processes", func(t *testing.T) {
		multiDir := t.TempDir() + "/multi"
		_ = os.MkdirAll(t.TempDir()+"/multi", 0700)

		var wg sync.WaitGroup
		wg.Add(2)

		go func() {
			defer wg.Done()
			_, err := generate(multiDir, "111", 3600, 0700)
			assert.NoError(t, err)
		}()

		go func() {
			defer wg.Done()
			_, err := generate(multiDir, "222", 3600, 0700)
			assert.NoError(t, err)
		}()

		wg.Wait()

		got, err := generate(multiDir, "333", 3600, 0700)
		assert.NoError(t, err)
		assert.Equal(t, "002", got)

		subDirs, _ := currentSubDirs(multiDir)
		assert.Len(t, subDirs, 3)
	})

	t.Run("re-used expired locks", func(t *testing.T) {
		expiredDir := t.TempDir() + "/expired"
		_ = os.MkdirAll(t.TempDir()+"/expired", 0700)

		var wg sync.WaitGroup
		wg.Add(1)

		go func() {
			defer wg.Done()
			_, err := generate(expiredDir, "111", -1000, 0700)
			assert.NoError(t, err)
		}()

		wg.Wait()

		got, err := generate(expiredDir, "222", 3600, 0700)
		assert.NoError(t, err)
		assert.Equal(t, "000", got)

		subDirs, _ := currentSubDirs(expiredDir)
		assert.Len(t, subDirs, 1)
	})
}

func TestRelease(t *testing.T) {
	testDir := t.TempDir()

	tests := map[string]struct {
		env envparser.ExecutorEnv

		prepDirs    func(*testing.T, envparser.ExecutorEnv)
		assertFiles func(*testing.T, envparser.ExecutorEnv)
	}{
		"missing directory": {
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: t.TempDir() + "/missing/000",
				},
			},
		},
		"release file": {
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "1001",
				},
				StatefulEnv: envparser.StatefulEnv{
					BaseDir:   testDir + "/release",
					BuildsDir: testDir + "/release/project_1/000",
				},
			},
			prepDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				got, err := generate(env.StatefulEnv.BaseDir+"/project_1", env.RequiredEnv.JobID, 3600, 0700)
				assert.NoError(t, err, "preparing test directories")
				assert.Equal(t, "000", got)
			},
			assertFiles: func(t *testing.T, env envparser.ExecutorEnv) {
				file, _ := open(env.StatefulEnv.BaseDir + "/project_1/.000.lock")
				ld := read(file)
				assert.Equal(t, "", ld.Expiration)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepDirs != nil {
				tt.prepDirs(t, tt.env)
			}

			Release(tt.env)

			if tt.assertFiles != nil {
				tt.assertFiles(t, tt.env)
			}
		})
	}
}

func TestCleanup(t *testing.T) {
	testDir := t.TempDir()

	tests := map[string]struct {
		gen configure.General
		env envparser.ExecutorEnv

		prepDirs    func(*testing.T, envparser.ExecutorEnv)
		assertError func(*testing.T, error)
		assertDirs  func(*testing.T, envparser.ExecutorEnv)
	}{
		"skip when MaxBuildDir == 0": {
			gen: configure.General{
				MaxBuildDir: 0,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"cleanup folder (single released)": {
			gen: configure.General{
				FFLimitBuildDir: true,
				MaxBuildDir:     1,
			},
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "1001",
				},
				StatefulEnv: envparser.StatefulEnv{
					BaseDir:   testDir + "/release",
					BuildsDir: testDir + "/release/project_1/000",
				},
			},
			prepDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				generateReleaseDirs(t, env, env.StatefulEnv.BaseDir+"/project_1")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				subs, _ := currentSubDirs(env.StatefulEnv.BaseDir + "/project_1")
				assert.Len(t, subs, 2)
			},
		},
		"cleanup folder (uncapped)": {
			gen: configure.General{
				FFLimitBuildDir:      true,
				MaxBuildDir:          1,
				UncapBuildDirCleanup: true,
			},
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "1001",
				},
				StatefulEnv: envparser.StatefulEnv{
					BaseDir:   testDir + "/release",
					BuildsDir: testDir + "/release/project_2/000",
				},
			},
			prepDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				generateReleaseDirs(t, env, env.StatefulEnv.BaseDir+"/project_2")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				subs, _ := currentSubDirs(env.StatefulEnv.BaseDir + "/project_2")
				assert.Len(t, subs, 1)
			},
		},
		"cleanup folder (default)": {
			gen: configure.General{
				FFLimitBuildDir: true,
				MaxBuildDir:     1,
			},
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "1001",
				},
				StatefulEnv: envparser.StatefulEnv{
					BaseDir:   testDir + "/release",
					BuildsDir: testDir + "/release/project_3/000",
				},
			},
			prepDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				generateReleaseDirs(t, env, env.StatefulEnv.BaseDir+"/project_3")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				subs, _ := currentSubDirs(env.StatefulEnv.BaseDir + "/project_3")
				assert.Len(t, subs, 2)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepDirs != nil {
				tt.prepDirs(t, tt.env)
			}

			err := Cleanup(tt.gen, tt.env)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertDirs != nil {
				tt.assertDirs(t, tt.env)
			}
		})
	}
}

func generateReleaseDirs(t *testing.T, env envparser.ExecutorEnv, baseDir string) {
	generate(baseDir, env.RequiredEnv.JobID, 3600, 0700)
	generate(baseDir, "2002", 3600, 0700)
	generate(baseDir, "3003", 3600, 0700)

	subs, _ := currentSubDirs(baseDir)
	assert.Len(t, subs, 3, "generate failed")

	Release(env)

	env.RequiredEnv.JobID = "2002"
	env.StatefulEnv.BuildsDir = baseDir + "/001"
	Release(env)

	env.RequiredEnv.JobID = "3003"
	env.StatefulEnv.BuildsDir = baseDir + "/002"
	Release(env)
}

func Test_requestLock(t *testing.T) {
	t.Run("missing lock file re-created", func(t *testing.T) {
		file, _ := open(t.TempDir() + "/test.lock")
		err := os.RemoveAll(file.Name())
		assert.NoError(t, err, "removing test file")

		newFile, err := requestLock(file)
		assert.NoError(t, err, "requesting lock")
		_, err = os.Stat(newFile.Name())
		assert.NoError(t, err, "verify file re-created")
	})
}

func Test_cleanupLock(t *testing.T) {
	t.Run("cleanup released file", func(t *testing.T) {
		testDir := t.TempDir()

		env := envparser.ExecutorEnv{
			RequiredEnv: envparser.RequiredEnv{
				JobID: "1234",
			},
			StatefulEnv: envparser.StatefulEnv{
				BaseDir:   testDir + "/release",
				BuildsDir: testDir + "/release/project_1/000",
			},
		}

		generate(testDir+"/release/project_1", env.RequiredEnv.JobID, -3600, 0700)
		Release(env)

		got := cleanupLock(testDir+"/release/project_1", "000")
		assert.True(t, got)
	})
}
