package flock

import (
	"errors"
	"os"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

/*
	The flock package is not supported outside of Linux, this implementation
	aims to help with local development/testing efforts only.
*/

func generate(dirname, jobID string, jobTimeout int, mode os.FileMode) (string, error) {
	return "", errors.New("not supported on Darwin")
}

func Release(env envparser.ExecutorEnv) {
	return
}

func Cleanup(gen configure.General, env envparser.ExecutorEnv) error {
	return errors.New("not supported on Darwin")
}
