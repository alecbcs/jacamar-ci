// Package flock leverages the underlying system call (flock(2)) in conjunction
// with pre-established rules to generate unique directories on supporting systems
// that will not conflict with other jobs running across a range of resources.
package flock

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
	"syscall"
	"time"

	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

type lockedData struct {
	JobID      string `json:"job_id"`
	Expiration string `json:"expiration"`

	file *os.File
}

// ManageLock maintains the workflow for generating a valid builds directory and returning
// the target via stdout if completed successfully.
func ManageLock(msg logging.Messenger, lCmd arguments.LockCmd, sysErr, exitZero func()) {
	mode := convertPerms(lCmd.DirPermissions)
	timeout, err := strconv.Atoi(lCmd.JobTimeout)
	if err != nil {
		msg.Stderr("Invalid timeout: %s", lCmd.JobTimeout)
		sysErr()
	}

	cDir, err := generate(lCmd.ProposedDir, lCmd.JobID, timeout, mode)
	if err != nil {
		msg.Stderr("Unable to generate lock: %s", err.Error())
		sysErr()
	}

	msg.Stdout(cDir)
	exitZero()
}

func convertPerms(dirPermissions string) os.FileMode {
	i, err := strconv.ParseUint(dirPermissions, 10, 64)
	if err != nil {
		return os.FileMode(usertools.OwnerPermissions)
	}

	return os.FileMode(uint32(i))
}

// ProposedBuildsDir uses the current base directory to identify and generate a new
// proposed location that can be used in locking workflow. Please note that no
// directory will be created or concurrent value identified.
func ProposedBuildsDir(baseDir string, claims gljobctx.Claims) string {
	index := strings.LastIndex(claims.ProjectPath, "/")
	projectName := strings.ToLower(claims.ProjectPath[index+1:])

	return baseDir + "/builds/" + projectName + "_" + claims.ProjectID
}

// open establishes RDWR access or generate the provided file.
func open(fileName string) (*os.File, error) {
	/* #nosec */
	// variable file path required
	return os.OpenFile(fileName, os.O_CREATE|os.O_RDWR, 0600)
}

// read examines the provided lock file and attempt to unmarshal the lockfile
// data. Regardless of error an empty structure will always be returned.
func read(file *os.File) *lockedData {
	var ld lockedData
	ld.file = file

	_, _ = file.Seek(0, 0)
	data := make([]byte, 0, 256)
	for {
		n, err := file.Read(data[len(data):cap(data)])
		data = data[:len(data)+n]
		if err != nil {
			if err == io.EOF {
				_ = json.Unmarshal(data, &ld)
			}
			break
		}
	}

	return &ld
}

func (l *lockedData) claim(jobTimeout int, jobID string) error {
	now := time.Now()
	dur := time.Second * time.Duration(jobTimeout+600)

	l.Expiration = strconv.FormatInt(now.Add(dur).Unix(), 10)
	l.JobID = jobID

	return l.update()
}

func (l *lockedData) release() error {
	l.Expiration = ""

	return l.update()
}

func (l *lockedData) update() error {
	var b []byte

	_, _ = l.file.Seek(0, 0)
	err := syscall.Truncate(l.file.Name(), 0)
	if err == nil {
		b, err = json.Marshal(l)
		if err == nil {
			_, err = l.file.Write(b)
		}
	}

	return err
}

// expired examines the identified lockfile date and determines if the expiration has been reached.
func (l *lockedData) expired() bool {
	if l.Expiration == "" {
		return true
	}

	now := time.Now()
	exp, _ := strconv.ParseInt(l.Expiration, 10, 64)

	return now.Unix() >= exp
}

// exists identifies if a file/folder is present.
func exists(name string) bool {
	_, err := os.Stat(name)

	return err == nil
}

// currentSubDirs returns a slice of subdirectories found within path.
func currentSubDirs(dirname string) (dirs []os.DirEntry, err error) {
	tmpDir, err := os.ReadDir(dirname)
	if err != nil {
		return
	}

	for _, d := range tmpDir {
		if d.IsDir() {
			dirs = append(dirs, d)
		}
	}
	sort.Slice(dirs, func(t, s int) bool {
		return dirs[t].Name() < dirs[s].Name()
	})

	return
}

// potentialLockTarget preforms a simple check on a files eligibility for a lock
// prior to any request. Not all potential cases can be account for; however,
// obvious cases where a lock is not required will be identified.
func potentialLockTarget(file *os.File) bool {
	ld := read(file)
	return ld.expired()
}

func statefulBaseName(state envparser.StatefulEnv) string {
	i := strings.LastIndex(state.BuildsDir, "/")

	return state.BuildsDir[0:i]
}

func statefulConcurrent(state envparser.StatefulEnv) string {
	return state.BuildsDir[len(state.BuildsDir)-3:]
}

func statefulLockName(state envparser.StatefulEnv) string {
	dir := statefulBaseName(state)
	concurrent := statefulConcurrent(state)

	return lockFileName(concurrent, dir)
}

func targetDirName(concurrent, dirname string) string {
	return fmt.Sprintf("%s/%s", dirname, concurrent)
}

func lockFileName(concurrent, dirname string) string {
	return fmt.Sprintf("%s/.%s.lock", dirname, concurrent)
}
