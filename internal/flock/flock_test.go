package flock

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

func createLock(t *testing.T) string {
	fileName := t.TempDir() + "/release.lock"
	f, _ := open(fileName)
	defer func() { _ = f.Close() }()

	_, _ = f.WriteString(`{"job_id":"123","expiration":"TEST"}`)

	return fileName
}

func Test_lockedData_interactions(t *testing.T) {
	tarFile := t.TempDir() + "/test.lock"

	t.Run("update/read modification to empty lock file", func(t *testing.T) {
		f, _ := open(tarFile)
		l := &lockedData{
			JobID:      "123",
			Expiration: "1234567890",
			file:       f,
		}
		err := l.update()
		assert.NoError(t, err)
		_ = f.Close()

		f, _ = open(tarFile)
		ll := read(f)
		assert.NotEmpty(t, ll)
		assert.Equal(t, l.Expiration, ll.Expiration)
		assert.Equal(t, l.JobID, ll.JobID)
		_ = f.Close()
	})

	t.Run("incorrect json file", func(t *testing.T) {
		badFile := t.TempDir() + "/invalid.lock"
		f, _ := os.Create(badFile)
		_, _ = f.Write([]byte("testing..."))
		_ = f.Close()

		ll := read(f)
		assert.Empty(t, ll.JobID)
		assert.Empty(t, ll.Expiration)
	})

	t.Run("release existing file", func(t *testing.T) {
		fileName := createLock(t)
		f, _ := open(fileName)

		l := read(f)
		err := l.release()
		assert.NoError(t, err)

		ll := read(f)
		assert.NoError(t, err)
		assert.Equal(t, "", ll.Expiration)
		assert.Equal(t, l.JobID, ll.JobID)
	})

	t.Run("claim existing file", func(t *testing.T) {
		fileName := createLock(t)
		f, _ := open(fileName)

		l := read(f)
		err := l.claim(300, "456")
		assert.NoError(t, err)
	})
}

func Test_currentSubDirs(t *testing.T) {
	tests := map[string]struct {
		dirname     string
		prepDirs    func(*testing.T, string)
		assertDirs  func(*testing.T, []os.DirEntry)
		assertError func(*testing.T, error)
	}{
		"no directory identified": {
			dirname: t.TempDir() + "/missing",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"empty directory": {
			dirname: t.TempDir() + "/empty",
			prepDirs: func(t *testing.T, s string) {
				_ = os.MkdirAll(s, 0700)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDirs: func(t *testing.T, dirs []os.DirEntry) {
				assert.Len(t, dirs, 0)
			},
		},
		"directory with files + folders": {
			dirname: t.TempDir() + "/test",
			prepDirs: func(t *testing.T, s string) {
				_ = os.MkdirAll(s+"/002", 0700)
				_ = os.MkdirAll(s+"/010", 0700)
				_ = os.MkdirAll(s+"/001", 0700)
				_, _ = os.CreateTemp(s, "file")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDirs: func(t *testing.T, dirs []os.DirEntry) {
				assert.Len(t, dirs, 3)
				assert.Equal(t, "001", dirs[0].Name())
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepDirs != nil {
				tt.prepDirs(t, tt.dirname)
			}

			gotFiles, err := currentSubDirs(tt.dirname)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertDirs != nil {
				tt.assertDirs(t, gotFiles)
			}
		})
	}
}

func Test_DirStructures(t *testing.T) {
	t.Run("ProposedBuildsDir - basic structure", func(t *testing.T) {
		got := ProposedBuildsDir("/ci/user", gljobctx.Claims{
			ProjectID:   "123",
			ProjectPath: "group/sub-group/project",
		})
		assert.Equal(t, "/ci/user/builds/project_123", got)
	})

	t.Run("statefulLockName", func(t *testing.T) {
		got := statefulLockName(envparser.StatefulEnv{
			BuildsDir: "/test/dir/project_1/000",
		})
		assert.Equal(t, "/test/dir/project_1/.000.lock", got)
	})
}

func Test_convertPerms(t *testing.T) {
	t.Run("fallback to 700", func(t *testing.T) {
		got := convertPerms("---")
		assert.Equal(t, os.FileMode(usertools.OwnerPermissions), got)
	})

	t.Run("correctly convert", func(t *testing.T) {
		got := convertPerms("488")
		assert.Equal(t, os.FileMode(0750), got)
	})
}

func Test_lockedData_expired(t *testing.T) {
	tests := map[string]struct {
		ld   *lockedData
		want bool
	}{
		"no expiration defined": {
			ld:   &lockedData{},
			want: true,
		},
		"expiration encountered": {
			ld: &lockedData{
				Expiration: "946713661",
			},
			want: true,
		},
		"not expired": {
			ld: &lockedData{
				Expiration: "2524636861",
			},
			want: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			assert.Equalf(t, tt.want, tt.ld.expired(), "expired()")
		})
	}
}
