package flock

import (
	"fmt"
	"os"
	"strconv"

	"golang.org/x/sys/unix"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

const (
	// maxConcurrent is an arbitrary value established to prevent potential infinite loops. Any
	// currently supported deployment would not be able to simultaneous run anywhere near this many
	// jobs at the same time.
	maxConcurrent = 999
)

// generate uses the file locking workflows to create a dedicated concurrent
// folder within the proposed directory that will be unique for the job's duration.
// If required the new directory with concurrent value post-fixed will be created
// (example: /test/directory -> /test/directory/004) and the utilized concurrent value
// is returned to support updating the auth processes identified builds_dir. Any created
// directory will observe the provided FileMode.
func generate(proposedDir, jobID string, jobTimeout int, mode os.FileMode) (string, error) {
	var targetConcurrent string
	concurrent := 0

	if !exists(proposedDir) {
		// This directory may need generated prior to prepare_exec. As such it cannot
		// be assumed the downscoped user has the necessary permissions.
		if err := os.MkdirAll(proposedDir, mode); err != nil {
			return "", fmt.Errorf("unable to generate proposed builds dir: %w", err)
		}
	}

	// Loop until we identify a valid directory or the maximum concurrent value is reached.
	for true {
		obtained, tmpCon, err := generateLock(proposedDir, jobID, concurrent, jobTimeout, mode)
		if err != nil {
			return "", fmt.Errorf(
				"failed to obtain .lock file in %s: %w",
				proposedDir,
				err,
			)
		} else if obtained {
			targetConcurrent = tmpCon
			break
		} else if concurrent == maxConcurrent {
			return "", fmt.Errorf("suprased maximum concurrent value (%v)", maxConcurrent)
		}

		concurrent++
	}

	return targetConcurrent, nil
}

func generateLock(
	proposedDir, jobID string,
	concurrent, jobTimeout int,
	mode os.FileMode,
) (bool, string, error) {
	targetCon := zeroPadConcurrent(concurrent)
	targetDir := targetDirName(targetCon, proposedDir)
	targetLock := lockFileName(targetCon, proposedDir)

	if !exists(targetDir) {
		// This directory may need generated prior to prepare_exec.
		err := os.MkdirAll(targetDir, mode)
		if err != nil {
			return false, "", fmt.Errorf("unable to generate proposed builds dir: %w", err)
		}
	}

	file, err := open(targetLock)

	// Only attempt to establish a lock if we are able to create/identify a file and
	// verify it is not already associated with an active job.
	if err == nil {
		if !potentialLockTarget(file) {
			return false, "", nil
		}

		file, err = requestLock(file)
		if err == nil {
			defer relinquishLock(file)

			ld := read(file)
			if !ld.expired() {
				return false, "", nil
			}
			err = ld.claim(jobTimeout, jobID)
		}
	}

	return err == nil, targetCon, err
}

// Release attempts to remove the expiration from the lock allowing potential re-use
// and cleanup of the target directory.
func Release(env envparser.ExecutorEnv) {
	// Errors identified either opening the file or obtaining the lock should result
	// in a skip. Future jobs that attempt to utilize these folders will encounter the
	// same error (if system/permission related) and the resulting message will be
	// better presented during prepare_exec.

	file, err := openLocked(statefulLockName(env.StatefulEnv))
	if err != nil {
		return
	}
	defer relinquishLock(file)

	ld := read(file)
	_ = ld.release()
}

// Cleanup removes expired subdirectories whose concurrent value exceeds the maximum allowed.
func Cleanup(gen configure.General, env envparser.ExecutorEnv) error {
	if gen.MaxBuildDir == 0 {
		// Zero indicate no maximum concurrent folder restrictions.
		return nil
	}

	targetDir := statefulBaseName(env.StatefulEnv)
	subDirs, err := currentSubDirs(targetDir)
	if err != nil {
		return err
	}

	if len(subDirs) > gen.MaxBuildDir {
		for i := gen.MaxBuildDir; i < len(subDirs); i++ {
			result := cleanupLock(targetDir, subDirs[i].Name())

			if result && !gen.UncapBuildDirCleanup {
				break
			}
		}
	}

	return nil
}

// cleanupLock attempts to remove the lock file and associated directory. If successful
// will return 'true'.
func cleanupLock(baseDir, subDir string) bool {
	file, err := openLocked(lockFileName(subDir, baseDir))
	if err != nil {
		return false
	}
	defer relinquishLock(file)

	if (read(file)).expired() {
		err = os.RemoveAll(fmt.Sprintf("%s/%s", baseDir, subDir))
		return err == nil
	}

	return false
}

func openLocked(fileName string) (*os.File, error) {
	file, err := open(fileName)
	if err == nil {
		file, err = requestLock(file)
	}

	return file, err
}

func zeroPadConcurrent(concurrent int) string {
	cID := strconv.Itoa(concurrent)
	for len(cID) < 3 {
		cID = "0" + cID
	}

	return cID
}

func requestLock(file *os.File) (newFile *os.File, err error) {
	err = lock(int(file.Fd()), unix.LOCK_EX)
	if err == nil {
		// Re-create file it was accidentally deleted while awaiting lock.
		if !exists(file.Name()) {
			newFile, err = open(file.Name())
		} else {
			newFile = file
		}
	}

	return
}

func relinquishLock(file *os.File) {
	if file != nil {
		/* #nosec */
		// Errors cannot be handled and no user notification possible.
		_ = lock(int(file.Fd()), unix.LOCK_UN)
		_ = file.Close()
	}
}

func lock(fd int, how int) error {
	return unix.Flock(fd, how)
}
