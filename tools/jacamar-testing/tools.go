// Package jacamartst maintains shared test functionality.
package jacamartst

import (
	"bytes"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/user"
	"runtime"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

// WriteTmpFile creates a temporary file with the provided prefix
// and the contents. Full file path returned if successful.
func WriteTmpFile(t *testing.T, prefix, contents string) string {
	file, _ := os.CreateTemp(t.TempDir(), prefix)

	text := []byte(contents)
	_, err := file.Write(text)
	assert.NoError(t, err, "error writing contents")

	_ = file.Close()
	_ = os.Chmod(file.Name(), 0700)

	return file.Name()
}

// NewURL implements the Mockgen Matcher interface  to compare a URL string to a URL found in
// a http.request structure.
func NewURL(u string) urlMatcher {
	y, _ := url.Parse(u)
	return urlMatcher{y: y}
}

type urlMatcher struct {
	y *url.URL
}

func (u urlMatcher) Matches(x interface{}) bool {
	return *u.y == *x.(*http.Request).URL
}

func (u urlMatcher) String() string {
	return fmt.Sprintf("is equal to %v", u.y)
}

// ClosingBuffer implements an io.ReaderCloser that
// always returns nil.
type ClosingBuffer struct {
	*bytes.Buffer
}

func (cb *ClosingBuffer) Close() error {
	return nil
}

// LinuxSkip skips current test if not running on Linux.
func LinuxSkip(t *testing.T) {
	if !strings.HasPrefix(runtime.GOOS, "linux") {
		t.Skip("tests only for root")
	}
}

// LinuxRootSkip skips current test is user is not root or not on linux.
func LinuxRootSkip(t *testing.T) {
	usr, _ := user.Current()
	if usr.Uid != "0" {
		t.Skip("tests only for root")
	}
}
