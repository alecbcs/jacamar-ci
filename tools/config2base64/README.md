# Config2Base64

Simply supports encoding a Jacamar configuration file into Base64.

```shell
$ go run config2base64.go example.toml
W2dlbmVyYWxdCiAgbmFtZ...
```

Alternatively reverse the process:

```shell
$ go run config2base64.go W2dlbmVyYWxdCiAgbmFtZ...
[general]
  name = "example config"
  ...
```
