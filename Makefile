SHELL := bash
.ONESHELL:
.DELETE_ON_ERROR:
.DEFAULT_GOAL := build
MAKEFLAGS += --no-builtin-rules

#
# Project & structure variables.
PKG = gitlab.com/ecp-ci/jacamar-ci
INTERNAL_PKG = ${PKG}/internal
COVER_REPORT ?= cover.out
BUILDDIR ?= binaries/
ROOT_DIR ?= $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif
INSTALL_PREFIX ?= $(if $(DESTDIR),$(DESTDIR)/$(PREFIX),$(PREFIX))

#
# CI/CD Variables used in local scripts
ifeq ($(CI_PROJECT_DIR),)
	export CI_PROJECT_DIR=${ROOT_DIR}
endif
ifeq ($(CI_PROJECT_NAME),)
	export CI_PROJECT_NAME=jacamar-ci
endif

#
# Identify container runtime support
ifeq ($(CONTAINER_RUNTIME),)
	export CONTAINER_RUNTIME=$(shell ./test/scripts/identify_container.bash)
endif

#
# Version variables.
GITCOMMIT := $(shell ./test/scripts/gitcommit.bash)
GITBRANCH := $(shell git rev-parse --abbrev-ref HEAD 2>/dev/null)
GOVERSION := $(shell go version | cut -c12- | awk '{ gsub (" ", "-", $$0); print}')
BUILDDATE := $(shell date -u +"%Y-%m-%dT%T%z")
ifndef GITCOMMIT
VERSION ?= $(shell cat VERSION)
else
VERSION ?= $(shell cat VERSION).pre.${GITCOMMIT}
endif
GOARCH := $(shell go env GOARCH)
GOOS := $(shell go env GOOS)

#
# Go variables.
GOCMD := go
CGO_ENABLED = 1
LDFLAGS = -X ${INTERNAL_PKG}/version.version=${VERSION} \
		  -X ${INTERNAL_PKG}/version.gitCommit=${GITCOMMIT} \
		  -X ${INTERNAL_PKG}/version.gitBranch=${GITBRANCH} \
		  -X ${INTERNAL_PKG}/version.goVersion=${GOVERSION} \
		  -X ${INTERNAL_PKG}/version.buildDate=${BUILDDATE} \
		  -s \
		  -w

include build/build.mk
include build/containers/container.mk
include build/package/package.mk
include test/test.mk
include test/pavilion/pav.mk

#
# Miscellaneous
.PHONY: clean
clean: #M Remove all temporary build, packaging, and testing objects.
	${GOCMD} clean
	rm -rf $(BUILDDIR) rpmbuild/ rpms/ strace-results/ bom/
	rm -f ${COVER_REPORT} cover.xml govulncheck.json
	rm -rf jacamar-ci-v*.tar.xz gitlab-runner-*.tar.xz
	$(MAKE) pav-container-clean

.PHONY: version
version: #M Display current version and Git details.
	@echo Version: ${VERSION}
	@echo Git Commit: ${GITCOMMIT}
	@echo Git Branch: ${GITBRANCH}
	@echo Go Version: ${GOVERSION}
	@echo Built: ${BUILDDATE}

.PHONY: mocks
mocks: #M Rebuild all mock interfaces (requires: https://github.com/golang/mock).
	bash ./test/scripts/mocks.bash

.PHONY: help
help:
	@printf "Usage: make [target] ..."
	@printf "\nBuild & Install:\n"
	@sed -ne '/@sed/!s/#B //p' $(MAKEFILE_LIST)
	@printf "\nTest:\n"
	@sed -ne '/@sed/!s/#T //p' $(MAKEFILE_LIST)
	@printf "\nPackage:\n"
	@sed -ne '/@sed/!s/#P //p' $(MAKEFILE_LIST)
	@printf "\nMiscellaneous:\n"
	@sed -ne '/@sed/!s/#M //p' $(MAKEFILE_LIST)
