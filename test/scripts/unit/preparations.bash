#!/usr/bin/env bash

# Prepare files/environment in advance of unit tests.

set -eo pipefail
set -o xtrace

chmod -R 700 test/scripts/unit
