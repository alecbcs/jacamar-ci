#!/bin/bash

user="$1"

if [ "$user" == "gitlab" ] ; then
  echo '{"username": "new", "user_message": "successful"}'
  exit 0
elif [ "$user" == "error" ] ; then
  echo '{"user_message": "error encountered", "error_message": "admin message"}'
  exit 1
fi

echo "no user argument provided"
exit 1
