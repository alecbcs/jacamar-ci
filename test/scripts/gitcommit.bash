#!/usr/bin/env bash

# Identify Git Commit for package version.

if git status >/dev/null 2>/dev/null ; then
    GITCOMMIT=$(git log --pretty=format:'%h' -n 1 2>/dev/null)$(git diff --quiet || echo '%')
fi
echo "${GITCOMMIT}"
