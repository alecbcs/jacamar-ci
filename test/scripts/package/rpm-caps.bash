#!/usr/bin/env bash

set -eo pipefail

[ -z "$CI_PROJECT_DIR" ] && CI_PROJECT_DIR=$(pwd)
[ -z "$CI_PROJECT_NAME" ] && CI_PROJECT_NAME="jacamar-ci"
specfile="build/package/rpm/runner-capabilities/jacamar-ci.spec"

tar cvfz "${RPMBUILD_DIR}/rpmbuild/SOURCES/jacamar-ci-v${VERSION}.tar.gz"\
  --exclude "${CI_PROJECT_NAME}/build/spack*" --exclude "${CI_PROJECT_NAME}/.*" \
  --exclude "${CI_PROJECT_NAME}/rpm*" \
  --transform "s/^${CI_PROJECT_NAME}/jacamar-ci-v${VERSION}/" ../${CI_PROJECT_NAME}

# In order to support potentially older build environment we must
# also define all variables here as opposed to relying on the defaults
# found in the spec file.
rpmbuild -bb "${CI_PROJECT_DIR}/${specfile}"  \
  --define "_topdir ${RPMBUILD_DIR}/rpmbuild" \
  --define "_version ${VERSION}" \
  --define "_user ${CAPABILITIES_USER}" \
  --define "_opt_prefix ${RPMOPT_PREFIX}" \
  --define "debug_package %{nil}"

cp -R "${RPMBUILD_DIR}"/rpmbuild/RPMS/"$(uname -m)"/* \
    "${CI_PROJECT_DIR}/rpms"
sha256sum "${CI_PROJECT_DIR}"/rpms/*
