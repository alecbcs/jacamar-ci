_cfg-env:
  variables:
    configs: $CI_PROJECT_DIR/test/pavilion/share/configs
    scripts: $CI_PROJECT_DIR/test/pavilion/share/scripts
    plugins: $CI_PROJECT_DIR/test/pavilion/share/go-plugins
    responses: $CI_PROJECT_DIR/test/pavilion/share/responses
  run:
    env:
      CUSTOM_ENV_CI_CONCURRENT_ID: 0
      CUSTOM_ENV_CI_JOB_ID: 123
      CUSTOM_ENV_CI_JOB_TOKEN: abc123efg456hij789kl
      CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN: token123
      CUSTOM_ENV_CI_SERVER_URL: http://127.0.0.1:5000
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt("default")}}'
      CUSTOM_ENV_CI_RUNNER_VERSION: '14.5.0-rc1'
      SYSTEM_FAILURE_EXIT_CODE: 2
      BUILD_FAILURE_EXIT_CODE: 1
      JOB_RESPONSE_FILE: '{{responses}}/valid_default.json'
      JACAMAR_CI_RUNNER_TIMEOUT: '1h0m0s'

_user-home-dir:
  inherits_from: _cfg-env
  run:
    env:
      JACAMAR_CI_BASE_DIR: /home/user/.jacamar-ci
      JACAMAR_CI_BUILDS_DIR: /home/user/.jacamar-ci/builds
      JACAMAR_CI_CACHE_DIR: /home/user/.jacamar-ci/cache
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/123
      JACAMAR_CI_AUTH_USERNAME: user
      JACAMAR_CI_PROJECT_PATH: group/project

_user-authorized-setuid:
  inherits_from: _user-home-dir
  run:
    env:
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/auth/user_authorized_setuid.toml")}}'

###############################################################################
#  misc/basic tests
###############################################################################

strace-version:
  inherits_from: _user-authorized-setuid
  summary: Check version of strace.
  run:
    cmds:
      - 'strace --version'

###############################################################################
# config_exec tests
###############################################################################

config-setuid-valid:
  inherits_from: _cfg-env
  summary: Load a basic valid configuration with a setuid run mechanism.
  run:
    cmds:
      - 'strace -f -c -o config-setuid-valid.strace jacamar-auth -u config --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      username:
        regex: '"JACAMAR_CI_AUTH_USERNAME":"user"'
        action: store_true
      builds_dir:
        regex: '"JACAMAR_CI_BASE_DIR":"/home/user/.jacamar-ci"'
        action: store_true
  result_evaluate:
    result: 'username and builds_dir and return_value == 0'

###############################################################################
# prepare_exec tests
###############################################################################

prepare-setuid-valid:
  inherits_from: _user-authorized-setuid
  summary: User's job environment prepared successfully.
  run:
    cmds:
      - 'strace -f -c -o prepare-setuid-valid.strace jacamar-auth prepare'
  result_parse:
    regex:
      output:
        regex: 'uid=\d*\(user\)'
        action: store_true
      groups:
        regex: 'groups=\d*\(user\),\d*\(ci\)'
  result_evaluate:
    result: 'output and return_value == 0'

###############################################################################
# run_exec tests
###############################################################################

run-setuid-valid:
  inherits_from: _user-authorized-setuid
  summary: User's job environment run successfully.
  run:
    cmds:
      - 'mkdir -p $JACAMAR_CI_SCRIPT_DIR && chown -R user:user /home/user'
      - 'touch /tmp/test.bash && echo date >> /tmp/test.bash'
      - 'strace -f -c -o run-setuid-valid.strace jacamar-auth run /tmp/test.bash prepare_script'
  result_parse:
    regex:
      output:
        regex: 'uid=\d*\(user\)'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

###############################################################################
# cleanup_exec tests
###############################################################################

cleanup-setuid-valid:
  inherits_from: _user-authorized-setuid
  summary: User's job environment cleanup successfully.
  run:
    cmds:
      - 'strace -f -c -o cleanup-setuid-valid.strace jacamar-auth cleanup --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      output:
        regex: 'uid=\d*\(user\)'
        action: store_true
  result_evaluate:
    result: 'output'
