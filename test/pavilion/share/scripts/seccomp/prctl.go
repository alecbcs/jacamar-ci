//go:build linux

package main

import (
	"fmt"
	"os"

	"golang.org/x/sys/unix"
)

func main() {
	v, err := unix.PrctlRetInt(
		unix.PR_GET_NO_NEW_PRIVS,
		uintptr(0),
		uintptr(0),
		uintptr(0),
		uintptr(0),
	)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Printf("PR_GET_NO_NEW_PRIVS: %v\n", v)
}
