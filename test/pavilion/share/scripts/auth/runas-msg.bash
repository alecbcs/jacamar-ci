#!/bin/bash

if [ "$#" -eq 1 ]; then
	user="$1"
	if [ "$user" == "user" ] ; then
		echo '{"user_message": "user message", "error_message": "admin message"}'
		exit 1
  fi
fi

echo "Failed RunAs Validation" >&2
exit 1
