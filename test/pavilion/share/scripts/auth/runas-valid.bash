#!/bin/bash

# Note, checksum validated, verify before updating.

if [ "$#" -eq 1 ]; then
	user="$1"
	if [ "$user" == "user" ] ; then
		echo '{"username": "newuser"}'
		exit 0
	elif [ "$user" == "jsonerror" ] ; then
		echo '"username": 123}'
		exit 0
	elif [ "$user" == "sharedgroup" ] ; then
		echo '{"username": "newuser", "sharedgroup": "test"}'
		exit 0
	elif [ "$user" == "norunas" ] ; then
		exit 0
  fi
elif [ "$#" -eq 2 ]; then
	user="$2"
	service_account="$1"
else
    exit 1
fi

if [ "$service_account" == "passtest" ] ; then
	exit 0
elif [ "$service_account" == "datadir" ] ; then
  echo '{"username": "newuser", "data_dir": "/runas/example"}'
  exit 0
fi

echo "Failed RunAs Validation" >&2
exit 1
