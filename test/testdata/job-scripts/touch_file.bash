#!/usr/bin/env bash

set -eo pipefail
set +o noclobber
: | eval $'touch /tmp/example-file\n'
exit 0
