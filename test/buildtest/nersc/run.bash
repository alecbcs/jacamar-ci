#!/usr/bin/env bash

# Prepare Buildtest deployment for NERSC specific tests.

set -eo pipefail

branch="v0.11.0"
dir="${HOME}/buildtest"

if [ -d "binaries/" ]; then
    echo "Using existing binaries directory"
else
  if [ $# -eq 0 ]; then
      echo "No jobid for artifact download provided."
      exit 1
  fi

  curl -L --output artifacts.zip https://gitlab.com/ecp-ci/jacamar-ci/-/jobs/$1/artifacts/download
  unzip artifacts.zip
  rm -rf artifacts.zip
fi

export PATH=$(pwd)/binaries:$PATH

# Python 3.7+ required
ml python

if [ -d ${dir} ]; then
    pushd ${dir}
      git fetch
      git checkout ${branch} 2>/dev/null || git checkout -b  ${branch}
    popd
else
    git clone --depth 1 --branch ${branch} https://github.com/buildtesters/buildtest.git ${dir}
fi

testdir="/global/cfs/cdirs/m3503/.ecp-ci/scratch"

# Prep test space
rm -rf ${testdir}
mkdir -p ${testdir}

python3 -m venv ${testdir}/env
source ${testdir}/env/bin/activate
pushd ${dir}
  source setup.sh
popd

# Avoid potentially using /tmp and stick to project space.
export TMPDIR=${testdir}

buildtest build -b test/buildtest/nersc/test.yml --testdir ${testdir}
