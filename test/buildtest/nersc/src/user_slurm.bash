#!/bin/bash

# test/buildtest/nersc/configs/user_slurm.toml
export JACAMAR_CI_CONFIG_STR="W2dlbmVyYWxdCiAgbmFtZSA9ICIiCiAgZXhlY3V0b3IgPSAic2x1cm0iCiAgZGF0YV9kaXIgPSAiL2dsb2JhbC9jZnMvY2RpcnMvbTM1MDMvLmVjcC1jaS9zY3JhdGNoIgogIHJldGFpbl9sb2dzID0gZmFsc2UKICBjdXN0b21fYnVpbGRfZGlyID0gdHJ1ZQogIGtpbGxfdGltZW91dCA9ICIiCiAgc2hlbGxfcGF0aCA9ICIiCiAgdmFyaWFibGVfZGF0YV9kaXIgPSBmYWxzZQogIGpvYl9tZXNzYWdlID0gIiIKICBnaXRsYWJfc2VydmVyID0gIiIKICB0bHMtY2EtZmlsZSA9ICIiCiAgZmZfY3VzdG9tX2RhdGFfZGlyID0gZmFsc2UKCltiYXRjaF0KICBhcmd1bWVudHNfdmFyaWFibGUgPSBbIlNMVVJNX0NJX1BBUkFNRVRFUlMiXQogIGNvbW1hbmRfZGVsYXkgPSAiMTVzIgogIG5mc190aW1lb3V0ID0gIjE1cyIKICBzY2hlZHVsZXJfYmluID0gIiIKICBza2lwX2NvYmFsdF9sb2cgPSBmYWxzZQoKW2F1dGhdCiAgZG93bnNjb3BlID0gIiIKICBqYWNhbWFyX3BhdGggPSAiIgogIGRvd25zY29wZV9jbWRfZGlyID0gIiIKICBtYXhfZW52X2NoYXJzID0gMAogIGxpc3RzX3ByZV92YWxpZGF0aW9uID0gZmFsc2UKICByb290X2Rpcl9jcmVhdGlvbiA9IGZhbHNlCiAgYWxsb3dfYm90X2FjY291bnRzID0gZmFsc2UKICBbYXV0aC5ydW5hc10KICAgIHZhbGlkYXRpb25fc2NyaXB0ID0gIiIKICAgIHZhbGlkYXRpb25fcGx1Z2luID0gIiIKICAgIHNoYTI1NiA9ICIiCiAgICB1c2VyX3ZhcmlhYmxlID0gIiIKICAgIEZlZGVyYXRlZCA9IGZhbHNlCiAgW2F1dGgubG9nZ2luZ10KICAgIGVuYWJsZWQgPSBmYWxzZQogICAgbG9nX2xldmVsID0gIiIKICAgIGxvY2F0aW9uID0gIiIKICAgIG5ldHdvcmsgPSAiIgogICAgYWRkcmVzcyA9ICIiCiAgW2F1dGguc2VjY29tcF0KICAgIGRpc2FibGVkID0gZmFsc2UKICAgIGJsb2NrX2FsbCA9IGZhbHNlCg=="

export CUSTOM_ENV_CI_CONCURRENT_ID=1
export CUSTOM_ENV_CI_JOB_ID=456
export CUSTOM_ENV_CI_JOB_TOKEN="abc123efg456hij789kl"
export CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN="token123"
export CUSTOM_ENV_CI_SERVER_URL="https://software.nersc.gov"
export CUSTOM_ENV_CI_JOB_JWT="placeholder.example.jwt"
export CUSTOM_ENV_CI_RUNNER_VERSION="14.2.0-rc1"

export SYSTEM_FAILURE_EXIT_CODE=2
export BUILD_FAILURE_EXIT_CODE=1

export JACAMAR_CI_BASE_DIR="/global/cfs/cdirs/m3503/.ecp-ci/scratch/.jacamar-ci"
export JACAMAR_CI_BUILDS_DIR="/global/cfs/cdirs/m3503/.ecp-ci/scratch/.jacamar-ci/builds"
export JACAMAR_CI_CACHE_DIR="/global/cfs/cdirs/m3503/.ecp-ci/scratch/.jacamar-ci/cache"
export JACAMAR_CI_SCRIPT_DIR="/global/cfs/cdirs/m3503/.ecp-ci/scratch/.jacamar-ci/scripts/nersc/slurm/456"
export JACAMAR_CI_PROJECT_PATH="nersc/slurm"
export JACAMAR_CI_AUTH_USERNAME=$(whoami)

export TESTDIR="/global/cfs/cdirs/m3503/.ecp-ci/scratch"
