%{!?_version: %define _version 0.10.1 }
%{!?_opt_prefix: %define _opt_prefix /opt }
%{!?_user: %define _user gitlab-runner }

# https://github.com/rpm-software-management/rpm/blob/master/macros.in
# build_id links are generated only when the __debug_package global is defined.
%define _build_id_links alldebug
%undefine _missing_build_ids_terminate_build

%global jac %{_opt_prefix}/jacamar
%global user %{_user}
%global group %{_user}
%global core_name jacamar-ci

Name:       %{core_name}-caps
Version:    %{_version}
Release:    1%{?dist}
Summary:    HPC focused CI/CD  driver for the GitLab custom executor for use with %{user} user and capabilities
Vendor:     Exascale Computing Project - 2.4.4.04
Packager:   ECP CI Infrastructure <ecp-ci-infrastructure@elist.ornl.gov>

# License details: https://gitlab.com/ecp-ci/jacamar-ci/-/blob/develop/LICENSE
License:    Apache 2.0 or MIT
URL:        https://gitlab.com/ecp-ci/%{core_name}
Source0:    https://gitlab.com/ecp-ci/%{core_name}/-/archive/v%{version}/%{core_name}-v%{version}.tar.gz

# Provides core project support, differentaited by configuration/deployment methods.
Provides:   %{core_name}

# Go is required for build but a newer version (see go.mod)
# than is available on most distributions.
BuildRequires:  make

Requires:   bash
Requires:   git
Requires:   gitlab-runner

Prefix: %{_opt_prefix}

%description
Jacamar CI is the HPC focused CI/CD driver for GitLab’s custom executor. The
core goal is to offer a maintainable, yet extensively configurable tool that
will allow for the use of GitLab’s robust testing model on unique HPC test
resources. Allowing teams to integrate potentially existing pipelines on
powerful scientific development environments.

%prep
%if %{defined _sha256sum}
    echo "%{_sha256sum}  %SOURCE0" | sha256sum -c -
%endif
%setup -q -n %{core_name}-v%{version}

%build
make build VERSION=%{version}

%pre
if ! id -u "%{user}" &>/dev/null; then
  echo "GitLab user (%{user}) not found on system, must be present for installation."
  exit 1
fi

# In limited cases SLES does not generate a "gitlab-runner" group during the
# runner installation. Though we enforce the existence of the user pre-install
# a missing group should be accounted for to avoid permission issues during
# supported configuration/workflows.
if ! getent group "%{group}" &>/dev/null; then
    groupadd --system %{group}
    usermod --gid %{group} %{user}
fi

%install
mkdir -p %{buildroot}%{jac}/bin
make install PREFIX=%{buildroot}%{jac}

%files
%attr(0555, root, root) %{jac}
%attr(0500, %{user}, %{group})  %{jac}/bin/jacamar-auth
%caps(cap_setuid,cap_setgid+ep) %{jac}/bin/jacamar-auth
