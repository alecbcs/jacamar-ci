%{!?_version: %define _version 0.10.1 }

# https://github.com/rpm-software-management/rpm/blob/master/macros.in
# build_id links are generated only when the __debug_package global is defined.
%define _build_id_links alldebug
%undefine _missing_build_ids_terminate_build

%global core_name jacamar-ci

Name:           %{core_name}-binaries
Version:        %{_version}
Release:        1%{?dist}
Summary:        HPC focused CI/CD driver for the GitLab custom executor, binaries only
Vendor:         Exascale Computing Project - 2.4.4.04
Packager:       ECP CI Infrastructure <ecp-ci-infrastructure@elist.ornl.gov>

# License details: https://gitlab.com/ecp-ci/jacamar-ci/-/blob/develop/LICENSE
License:        Apache 2.0 or MIT
URL:            https://gitlab.com/ecp-ci/%{core_name}
Source0:        https://gitlab.com/ecp-ci/%{core_name}/-/archive/v%{version}/%{core_name}-v%{version}.tar.gz

# Provides core project support, differentaited by configuration/deployment methods.
Provides:   %{core_name}

# Go is required for build but a newer version (see go.mod)
# than is available on most distributions.
BuildRequires:  make

Requires:   bash
Requires:   git
Requires:   gitlab-runner

Prefix: %{_prefix}

%description
Jacamar CI is the HPC focused CI/CD driver for GitLab's custom executor. The
core goal is to offer a maintainable, yet extensively configurable tool that
will allow for the use of GitLab's robust testing model on unique test
resources. Allowing teams to integrate existing pipelines onto powerful
development environments.

%prep
%if %{defined _sha256sum}
    echo "%{_sha256sum}  %SOURCE0" | sha256sum -c -
%endif
%setup -q -n %{core_name}-v%{version}

%build
make build VERSION=%{version}

%install
make install PREFIX=%{buildroot}%{_prefix}

%files
%attr(0755, root, root) %{_bindir}/jacamar
%attr(0755, root, root) %{_bindir}/jacamar-auth
