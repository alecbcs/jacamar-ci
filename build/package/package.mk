RPMBUILD_DIR ?= "${HOME}"
RPMOPT_PREFIX ?= "/opt"
RPM_PREFIX ?= "/usr"
CAPABILITIES_USER ?= "gitlab-runner"

ci_dir="/builds/ecp-ci/jacamar-ci"

.PHONY: release
release: #P Generate release RPM directory/tar locally.
	$(MAKE) rpmdev-setup
	VERSION=${VERSION} RPMBUILD_DIR=${RPMBUILD_DIR} RPMOPT_PREFIX=${RPMOPT_PREFIX} \
		bash ./test/scripts/package/rpm-std.bash

.PHONY: release-caps
release-caps: #P Generate release RPM directory/tar locally for official runner deployments with capabilities.
	$(MAKE) rpmdev-setup
	VERSION=${VERSION} RPMBUILD_DIR=${RPMBUILD_DIR} RPMOPT_PREFIX=${RPMOPT_PREFIX} \
	CAPABILITIES_USER=${CAPABILITIES_USER} \
		bash ./test/scripts/package/rpm-caps.bash

.PHONY: release-binaries
release-binaries: #P Generate release RPM directory/tar locally for binary only RPM.
	$(MAKE) rpmdev-setup
	VERSION=${VERSION} RPMBUILD_DIR=${RPMBUILD_DIR} RPM_PREFIX=${RPM_PREFIX} \
		bash ./test/scripts/package/rpm-bin.bash

.PHONY: rpmdev-setup
rpmdev-setup:
	$(MAKE) clean
	mkdir -p ${RPMBUILD_DIR}/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
	mkdir -p ${CI_PROJECT_DIR}/rpms

.PHONY: rpm-container
rpm-container: #P Generate RPM directory/tar via a container.
	echo "Running RPM Build in ${CONTAINER_RUNTIME}..."
	${CONTAINER_RUNTIME} run \
		--rm \
		-v "${CI_PROJECT_DIR}:${ci_dir}:z" \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-e "CI_PROJECT_NAME=${CI_PROJECT_NAME}" \
		-e "RPMOPT_PREFIX=${RPMOPT_PREFIX}" \
		-e "VERSION=${VERSION}" \
		-w ${ci_dir} \
		-t ${BUILD_IMG} \
		bash -c "make release"

.PHONY: rpm-caps-container
rpm-caps-container: #P Generate RPM directory/tar via a container for official runner deployments with capabilities.
	echo "Running RPM Caps Build in ${CONTAINER_RUNTIME}..."
	${CONTAINER_RUNTIME} run \
		--rm \
		-v "${CI_PROJECT_DIR}:${ci_dir}:z" \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-e "CI_PROJECT_NAME=${CI_PROJECT_NAME}" \
		-e "RPMOPT_PREFIX=${RPMOPT_PREFIX}" \
		-e "CAPABILITIES_USER=${CAPABILITIES_USER}" \
		-e "VERSION=${VERSION}" \
		-w ${ci_dir} \
		-t ${BUILD_IMG} \
		bash -c "make release-caps"

.PHONY: rpm-container-binaries
rpm-container-binaries: #P Generate RPM directory/tar via a container for binary only RPM.
	echo "Running RPM Build in ${CONTAINER_RUNTIME}..."
	${CONTAINER_RUNTIME} run \
		--rm \
		-v "${CI_PROJECT_DIR}:${ci_dir}:z" \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-e "CI_PROJECT_NAME=${CI_PROJECT_NAME}" \
		-e "RPM_PREFIX=${RPM_PREFIX}" \
		-e "VERSION=${VERSION}" \
		-w ${ci_dir} \
		-t ${BUILD_IMG} \
		bash -c "make release-binaries"

############################################################
# Keep Docker commands to support backwards compatability. #
############################################################

.PHONY: rpm-docker
rpm-docker:
	CONTAINER_RUNTIME=docker $(MAKE) rpm-container

.PHONY: rpm-caps-docker
rpm-caps-docker:
	CONTAINER_RUNTIME=docker $(MAKE) rpm-caps-container
