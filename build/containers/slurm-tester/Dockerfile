# Slurm Docker Container on CentOS 7: https://github.com/giovtorres/docker-centos7-slurm
ARG SLURM_VERSION
FROM giovtorres/docker-centos7-slurm:${SLURM_VERSION} AS build

RUN useradd -ms /bin/bash user \
    && echo "user ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

RUN yum update -y -q \
    && yum install -y -q \
        curl-devel \
        gettext-devel \
        libseccomp-devel \
        perl-CPAN \
        rsyslog \
    && yum remove -y git \
    && yum clean all \
    && touch /var/log/syslog \
    && mkdir /ecp \
    && chmod 777 /ecp

ENV PYENV_VERSION "3.7.12"
ARG PAV_TAG
RUN python --version \
    && git clone https://github.com/hpc/pavilion2.git \
    && cd pavilion2 \
        && git checkout ${PAV_TAG} \
        && git submodule update --init --recursive \
        && mv bin/* /usr/local/bin/ \
        && mv lib/* /usr/local/lib/ \
        && mv RELEASE.txt /usr/local/ \
    && cd .. \
    && rm -rf pavilion2 \
    && chmod -R 755 /usr/local/bin \
    && chmod -R 755 /usr/local/lib \
    && pip install pyyaml flask pyjwt[crypto] \
    # Allow easier access to Slurm container installed Python versions and scripts.
    && chmod -R 755 /root \
    && chmod 777 /root/.pyenv

VOLUME ["/var/lib/mysql", "/var/lib/slurmd", "/var/spool/slurm", "/var/log/slurm"]

ENTRYPOINT ["/tini", "--", "/usr/local/bin/docker-entrypoint.sh"]
CMD ["/bin/bash"]
