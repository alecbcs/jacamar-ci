# https://github.com/containers/podman
ARG PODMAN_VERSION
FROM quay.io/podman/stable:v${PODMAN_VERSION}

WORKDIR /

ENV PATH "/root/.pyenv/shims:/root/.pyenv/bin:/root/.local/bin:/root/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
ENV PYENV_VERSION "3.7.12"

RUN dnf -y update \
    && dnf install -y \
        bzip2-devel \
        curl \
        g++ \
        git \
        gpg \
        hostname \
        libseccomp-devel \
        ncurses-devel \
        openssl-devel \
        patch \
        readline-devel \
        which \
        xz-devel \
        zlib-devel \
    && dnf clean all \
    && curl https://pyenv.run | bash \
    && echo "eval \"\$(pyenv init --path)\"" >> "${HOME}/.bashrc" \
    && echo "eval \"\$(pyenv init -)\"" >> "${HOME}/.bashrc" \
    && source "${HOME}/.bashrc" \
    && pyenv update \
    && pyenv install ${PYENV_VERSION} \
    && pyenv global ${PYENV_VERSION} \
    && pip install pyyaml flask pyjwt[crypto] \
    && useradd -m user

ARG PAV_TAG
RUN python --version \
    && git clone https://github.com/hpc/pavilion2.git \
    && cd pavilion2 \
        && git checkout ${PAV_TAG} \
        && git submodule update --init --recursive \
        && mv bin/* /usr/local/bin/ \
        && mv lib/* /usr/local/lib/ \
        && mv RELEASE.txt /usr/local/ \
    && cd .. \
    && rm -rf pavilion2 \
    && chmod -R 755 /usr/local/bin \
    && chmod -R 755 /usr/local/lib \
    # Allow easier access to Slurm container installed Python versions and scripts.
    && chmod -R 755 /root \
    && chmod 777 /root/.pyenv

ARG GO_VERSION
ARG GO_ARCH
ARG GO_SHA
ARG GO_URL
ENV GOPATH /go

RUN mkdir -p "$GOPATH/"{src,bin} \
    && chmod -R 777 "$GOPATH" \
    && curl -L -o go.tgz "${GO_URL}" \
	&& echo "${GO_SHA} *go.tgz" | sha256sum -c - \
	&& tar -C /usr/local -xzf go.tgz \
	&& rm go.tgz \
    && mkdir /builds \
    && chmod 777 /builds

ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH
