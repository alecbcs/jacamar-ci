package main

import (
	"log"
	"os"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/cmd/jacamar"
)

func main() {
	c := arguments.Resolve(false)

	if !c.NoAuth {
		// Avoid configurations where jacamar is used as opposed to jacamar-auth, leading
		// to a lack of downscoping.
		log.Fatal("Jacamar requires --no-auth in order to support execution requirements.")
	}

	jacamar.Start(c)

	os.Exit(0)
}
