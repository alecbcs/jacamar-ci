package main

import (
	"os"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/cmd/auth"
)

func main() {
	auth.Start(arguments.Resolve(true))

	os.Exit(0)
}
